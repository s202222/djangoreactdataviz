"""django_react_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from backend.views import base_app
from backend.views import esc_app

# API urls of the app
urlpatterns = [
    path("dataset", base_app.dataset, name="dataset3"),
    path("filtered_csv", base_app.get_filtered_csv, name="filteredCSV"),
    path("filter_options", base_app.filter_options, name="filterOptions"),
    path("line_chart_data", base_app.line_chart_data, name="lineChartData"),
    path("get_year_range", base_app.get_year_range, name="yearRange"),
    path("esc/filtered_csv", esc_app.get_filtered_csv, name="filteredCSVEsc"),
    path("esc/filter_options", esc_app.filter_options, name="filterOptionsEsc"),
    path("esc/chart_data", esc_app.chart_data, name="escChartData"),
    path("esc/get_year_range", esc_app.get_year_range, name="escYearRange"),
]
