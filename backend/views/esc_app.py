from django.http import QueryDict, JsonResponse, HttpResponse
from rest_framework.decorators import api_view
import pandas as pd
from datetime import datetime
import codecs


# Merge Other and Not available data under 1 label
def format_df(df):
    df['ESC_phenotype'] = df['ESC_phenotype'].replace(
        {'Not available': 'Other/Not available', 'Other': 'Other/Not available'}
    )
    return df


# The main dataframe to work with
df_esc = format_df(pd.read_csv('backend/static/backend/data/danmap_ESC_select.csv'))
# Columns to be filtered
columns = ["KATEGORI", "ORIGIN"]
# Corresponding query params for the columns to be filtered
params = ["category", "origin"]


# Returns with the widest available year range in the data
@api_view()
def get_year_range(request):
    year_range = {
        "minYear": int(df_esc['AAR'].min()),
        "maxYear": int(df_esc['AAR'].max()),
    }
    return JsonResponse(year_range, safe=False)


# Returns with the data needed to display the chart, in the right format
@api_view()
def chart_data(request):
    category_params = request.query_params.getlist('category')
    origin_params = request.query_params.getlist('origin')

    from_year = int(request.query_params.get('fromYear', int(df_esc['AAR'].min())))
    to_year = int(request.query_params.get('toYear', int(df_esc['AAR'].max())))

    final_data = {
        "data": [],
        "legendLabels": [],
        "numberOfSamples": [],
        "numberOfSamplesXAxis": {},
    }

    for i in range(len(category_params)):
        if i != 0:
            final_data['data'].append({'AAR': f'{i}'})

        category = category_params[i]
        origin = origin_params[i]

        sub_chart_data = generate_sub_chart_data(
            QueryDict(f'category={category}&origin={origin}'), from_year, to_year, i)
        for attr, value in final_data.items():
            if type(value) == dict:
                final_data[attr].update(sub_chart_data[attr])
            else:
                final_data[attr].extend(sub_chart_data[attr])

    # Remove duplicates
    final_data["legendLabels"] = list(set(final_data["legendLabels"]))

    return JsonResponse(final_data, safe=False)


# Returns the available options in the filter dropdowns
@api_view()
def filter_options(request):
    category_params = request.query_params.getlist('category')
    origin_params = request.query_params.getlist('origin')

    final_filter_options = []

    for i in range(len(category_params)):
        category = category_params[i]
        origin = origin_params[i]

        sub_chart_filter_options = generate_sub_chart_filter_options(QueryDict(f'category={category}&origin={origin}'))
        final_filter_options.append(sub_chart_filter_options)

    return JsonResponse(final_filter_options, safe=False)


# Get the filtered dataset in CSV which can be downloaded by the user
@api_view()
def get_filtered_csv(request):
    df_filtered = filter_df(request.query_params)
    response = HttpResponse(content_type='text/csv')
    response[
        'Content-Disposition'] = f'attachment; filename=danmap_ESC_filtered_{datetime.now().strftime("%Y%m%d-%H%M%S")}.csv'
    # Creates the BOM in the beginning of the file, so Excel can recognize the utf-8 encoding automatically when importing the CSV
    response.write(codecs.BOM_UTF8)
    df_filtered.to_csv(path_or_buf=response, index=False, sep=';', encoding='utf-8', decimal=',')
    return response


# Get filter options for one subchart filter pair (category + origin)
def generate_sub_chart_filter_options(query_params):
    # Filter to relevant columns (KATEGORI, ORIGIN)
    df_reduced = df_esc[columns]

    options = {}

    def _is_subset(subframe, _y, query_param_l):
        return set(query_param_l).issubset(set(subframe[columns[_y]]))

    for x in range(len(columns)):
        # Group by current column (e.g. group by KATEGORI)
        grouped = df_reduced.groupby(columns[x])

        # Always filter down the table and save the latest filtered one. The starting table is the "grouped"
        filtered = grouped
        # Check for the remaining columns' filter params if they're included in the grouped lists
        # E.g. if the dataframe is grouped by animal category, check whether the origin query param list is
        # included in the ORIGIN list
        for y in range(len(columns)):
            if columns[x] != columns[y]:
                query_param_list = query_params.getlist(params[y])
                # If query param is set for the column (i.e. query param list is not empty)
                if query_param_list:
                    filtered_groups = filtered.filter(lambda subframe: _is_subset(subframe, y, query_param_list))
                    # Filter again to only keep the matching rows (matching with the query params)
                    filtered_values = filtered_groups[filtered_groups[columns[y]].isin(query_param_list)]
                    # Group the table again to be able to execute the filter again in the next iteration
                    filtered = filtered_values.groupby(columns[x])

        options[params[x]] = list(filtered.groups.keys())

    return options


# Generate chart data for one subchart
def generate_sub_chart_data(query_params: QueryDict, from_year: int, to_year: int, i: int):
    df_filtered = filter_df(query_params)
    df_groupby_year = df_filtered.groupby('AAR').agg(lambda x: list(x))

    sub_chart_data = {
        "data": [],
        "legendLabels": [],
        "numberOfSamples": [],
        "numberOfSamplesXAxis": {},
    }

    legend_label_set = set()

    for year in range(from_year, to_year + 1):
        if year in df_groupby_year.index:
            sub_chart_data["data"].append({
                "AAR": f'{year}_{i}',
            })
            sub_chart_data["numberOfSamples"].append({
                "AAR": f'{year}_{i}',
            })
            row = df_groupby_year.loc[year]
            data, number_of_samples = {}, {}
            df_in_year = df_filtered[df_filtered['AAR'] == year]
            df_ph_grouped = df_in_year.groupby('ESC_phenotype').agg(lambda x: list(x))
            for ph in df_ph_grouped.index:
                row_ph = df_ph_grouped.loc[ph]
                legend_label = f"{ph} phenotype" if ph in ['ESBL', 'AmpC', 'ESBL-AmpC'] else f"{ph}"
                data[legend_label] = round((len(row_ph['pct_ESC']) / row_ph['N'][0]) * 100, 1)
                number_of_samples[legend_label] = len(row_ph['pct_ESC'])
                legend_label_set.add(legend_label)
            sub_chart_data["data"][len(sub_chart_data["data"]) - 1].update(data)
            sub_chart_data["numberOfSamples"][len(sub_chart_data["numberOfSamples"]) - 1].update(number_of_samples)
    sub_chart_data["legendLabels"] = list(legend_label_set)

    sub_chart_data['numberOfSamplesXAxis'] = get_number_of_samples_in_year(df_filtered, from_year, to_year, i)

    return sub_chart_data


# Filter dataframe on above defined columns
def filter_df(query_params: QueryDict):
    df_esc_base = df_esc.copy()
    filter_condition = [True] * len(df_esc_base.index)
    for i in range(len(columns)):
        filter_condition = filter_condition & (
            df_esc_base[columns[i]].isin(query_params.getlist(params[i])))

    df_esc_filtered = df_esc_base[filter_condition]

    return df_esc_filtered


# Get the number of samples in each year
def get_number_of_samples_in_year(df, from_year, to_year, i):
    samples_in_year = {}
    samples_in_year_df = df[['AAR', 'N']][df['AAR'].isin(range(from_year, to_year + 1))].groupby('AAR')[
        'N'].max().reset_index()
    for year in range(from_year, to_year + 1):
        if year in list(samples_in_year_df['AAR']):
            samples_in_year[f'{year}_{i}'] = int(samples_in_year_df[samples_in_year_df['AAR'] == year]['N'])
        else:
            samples_in_year[f'{year}_{i}'] = 0
    return samples_in_year
