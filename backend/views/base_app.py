import codecs
from datetime import datetime
import pandas as pd
from django.http import JsonResponse, QueryDict, HttpResponse
from rest_framework.decorators import api_view

datamap_df = pd.read_csv('backend/static/backend/data/danmap.csv', ";", decimal=",")
datamap_df = datamap_df.replace(
        {"ORIGIN": {"Domestically acquired": "Domestic cases", "Travel abroad reported": "Travel cases"}})
columns = ["FUND", "DYRE_KATEGORI", "ORIGIN", "ANTIBIOTIKA"]
params = ["finding", "animal", "origin", "antibiotic"]


# Generate filtered dataframe
def get_clean_dataframe(query_params: QueryDict, all_columns: bool):
    # Pick an animal (import or danish) - Beef - Danish
    # Pick a certain bacterium - Escherichia coli spp.
    # Pick a drug which is resistant to - Ampicillin
    # Plot it: x-axis: AAR, y-axis: Pct_Res

    # Dataframe with only valid values
    datamap_df_base = datamap_df[datamap_df['Pct_Res'].notnull()]

    filter_condition = [True] * len(datamap_df_base.index)
    for i in range(len(columns)):
        filter_condition = filter_condition & (
            datamap_df_base[columns[i]].isin(query_params.getlist(params[i])))

    datamap_filtered = datamap_df_base[filter_condition]

    if all_columns:
        return datamap_filtered
    else:
        compare_by_list = query_params.getlist('compare-by')
        compare_by_cols = map(lambda compareby: columns[params.index(compareby)], compare_by_list)
        datamap_reduced = datamap_filtered[['AAR', 'Pct_Res', 'N', *list(compare_by_cols)]]
        datamap_reduced = datamap_reduced.round(1)
        return datamap_reduced


# Get the number of samples in each year
def get_number_of_samples_in_year(df, from_year, to_year):
    samples_in_year = {}
    samples_in_year_df = df[['AAR', 'N']][df['AAR'].isin(range(from_year, to_year+1))].groupby('AAR')['N'].max().reset_index()
    for year in range(from_year, to_year+1):
        if year in list(samples_in_year_df['AAR']):
            samples_in_year[year] = int(samples_in_year_df[samples_in_year_df['AAR'] == year]['N'])
        else:
            samples_in_year[year] = 0
    return samples_in_year


# Get widest available range of years from data
@api_view()
def get_year_range(request):
    year_range = {
        "minYear": int(datamap_df['AAR'].min()),
        "maxYear": int(datamap_df['AAR'].max()),
    }
    return JsonResponse(year_range, safe=False)


# Get the filtered dataset in CSV which can be downloaded by the user
@api_view()
def get_filtered_csv(request):
    datamap_reduced = get_clean_dataframe(request.query_params, all_columns=True)
    response = HttpResponse(content_type='text/csv')
    response[
        'Content-Disposition'] = f'attachment; filename=danmap_filtered_{datetime.now().strftime("%Y%m%d-%H%M%S")}.csv'
    # Creates the BOM in the beginning of the file, so Excel can recognize the utf-8 encoding automatically when importing the CSV
    response.write(codecs.BOM_UTF8)
    datamap_reduced.to_csv(path_or_buf=response, index=False, sep=';', encoding='utf-8', decimal=',')
    return response


# Get filter options with AND relationship between values
@api_view()
def filter_options(request):
    datamap_df_base = datamap_df[datamap_df['Pct_Res'].notnull()]

    # Only select the columns which are relevant
    df_reduced = datamap_df_base[columns]

    options = {}

    def _is_subset(subframe, _y, query_param_l):
        return set(query_param_l).issubset(set(subframe[columns[_y]]))

    def has_intersect(subframe, _y, query_param_l):
        return bool(set(query_param_l) & set(subframe[columns[_y]]))

    def is_multi_select_column(column):
        return column in list(
            map(lambda compareby: columns[params.index(compareby)], request.query_params.getlist('compare-by')))

    # Iterate over each column
    for x in range(len(columns)):
        # Group by current column (e.g. group by DYRE_KATEGORI)
        grouped = df_reduced.groupby(columns[x])

        # Always filter down the table and save the latest filtered one. The starting table is the "grouped"
        filtered = grouped
        # Check for the remaining columns' filter params if they're included in the grouped lists
        # E.g. if the dataframe is grouped by animals, check whether the other 3 query param lists are
        # included in the FUND/ORIGIN/ANTIBIOTIKA lists
        for y in range(len(columns)):
            if columns[x] != columns[y]:
                query_param_list = request.query_params.getlist(params[y])
                # If query param is set for the column (i.e. query param list is not empty)
                if query_param_list:
                    # If the column for which we collect the possible filter options is a "compare-by" column
                    if is_multi_select_column(columns[x]):
                        # Only keep the origins with list that intersects the query_param_list of the second column
                        # E.g. if column_x is the ORIGIN, only keep the ORIGIN groups whose list has at least 1 element
                        # from the query param list (e.g. has at least Cattle or Human if the column_y is DYRE_KATEGORI)
                        filtered_groups = filtered.filter(lambda subframe: has_intersect(subframe, y, query_param_list))
                        # Filter again to only keep the matching rows (matching with the query params)
                        filtered_values = filtered_groups[filtered_groups[columns[y]].isin(query_param_list)]
                        # Group the table again to be able to execute the filter again in the next iteration
                        filtered = filtered_values.groupby(columns[x])
                    else:
                        filtered_groups = filtered.filter(lambda subframe: _is_subset(subframe, y, query_param_list))
                        # Filter again to only keep the matching rows (matching with the query params)
                        filtered_values = filtered_groups[filtered_groups[columns[y]].isin(query_param_list)]
                        # Group the table again to be able to execute the filter again in the next iteration
                        filtered = filtered_values.groupby(columns[x])

        options[params[x]] = list(filtered.groups.keys())

    return JsonResponse(options, safe=False)


# Process query params from request
def extract_query_param_values(query_params):
    from_year = int(query_params.get('fromYear'))
    to_year = int(query_params.get('toYear'))
    cols_to_compare = list(
        map(lambda compareby: columns[params.index(compareby)], query_params.getlist('compare-by')))
    return from_year, to_year, cols_to_compare


# Get data to display the line chart
@api_view()
def line_chart_data(request):
    datamap_reduced = get_clean_dataframe(request.query_params, all_columns=False)
    from_year, to_year, cols_to_compare = extract_query_param_values(request.query_params)
    datamap_groupby = datamap_reduced.groupby(cols_to_compare).agg(lambda x: list(x)).reset_index()
    # Set custom order on column_to_compare, in order to return with data in the same order
    # as the values were selected in the dropdown. This is needed to assign the right colors on the frontend
    # for col in cols_to_compare:
    #     datamap_groupby[col] = pd.Categorical(datamap_groupby[col], request.query_params.getlist(params[columns.index(col)]))
    #     # Sort data
    #     datamap_groupby = datamap_groupby.sort_values(col)

    # Sorting by year might prevent nivo to display the year in mixed order sometimes
    datamap_groupby = datamap_groupby.sort_values('AAR')

    # Step 1: groupby compare-by option, 1 group corresponds to 1 line, e.g. by antibiotic
    # Step 2: id = name of the antibiotic/animal etc.
    # Step 3: Create 'data' array by iterating over the list of values belongs to the group
    # Step 4: Set x to the year
    # Step 5: Set y to the percentage

    final_data = {
        "data": [],
        "legendLabels": [],
        "chartType": "line",
        "numberOfSamples": [],
        "numberOfSamplesXAxis": {},
    }

    # index = Antibiotika, row = Aar - Pct_Res
    for index, row in datamap_groupby.iterrows():
        data, number_of_samples = [], []
        for year in range(from_year, to_year+1):
            if year in row['AAR']:
                idx = row['AAR'].index(year)
                data.append({
                    "x": year,
                    "y": row['Pct_Res'][idx]
                })
                number_of_samples.append({
                    "year": year,
                    "samples": row['N'][idx]
                })
            else:
                data.append({"x": year, "y": None})

        # Indexing is valid until the max values to compare by is 2
        legend_label = f"{row[cols_to_compare[0]]} - {row[cols_to_compare[1]]}" if len(
            request.query_params.getlist('compare-by')) == 2 else f"{row[cols_to_compare[0]]}"
        if row[cols_to_compare[0]] == 'Human':
            legend_label = legend_label.split(' ', 2)[2]
        final_data["legendLabels"].append(legend_label)
        final_data["data"].append({
            "id": legend_label,
            "data": data
        })
        final_data["numberOfSamples"].append({
            "id": legend_label,
            "numberOfSamples": number_of_samples
        })
    final_data['numberOfSamplesXAxis'] = get_number_of_samples_in_year(datamap_reduced, from_year, to_year)

    return JsonResponse(final_data, safe=False)


# Get dataset in JSON format (the dataset that needs to be displayed in the bar chart)
@api_view()
def dataset(request):
    datamap_reduced = get_clean_dataframe(request.query_params, all_columns=False)
    from_year, to_year, cols_to_compare = extract_query_param_values(request.query_params)
    datamap_groupby_year = datamap_reduced.groupby('AAR').agg(lambda x: list(x))

    final_data = {
        "data": [],
        "legendLabels": [],
        "chartType": "bar",
        "numberOfSamples": [],
        "numberOfSamplesXAxis": {},
    }
    # index = Aar, row = Antibiotika - Pct_Res
    legend_label_set = set()
    for year in range(from_year, to_year+1):
        final_data["data"].append({
            "AAR": year,
        })
        final_data["numberOfSamples"].append({
            "AAR": year,
        })
        if year in datamap_groupby_year.index:
            row = datamap_groupby_year.loc[year]
            compare_by_object, number_of_samples = {}, {}
            for i in range(len(row['Pct_Res'])):
                legend_label = f"{row[cols_to_compare[0]][i]} - {row[cols_to_compare[1]][i]}" if len(
                    request.query_params.getlist('compare-by')) == 2 else f"{row[cols_to_compare[0]][i]}"
                compare_by_object[legend_label] = row['Pct_Res'][i]
                number_of_samples[legend_label] = row['N'][i]
                legend_label_set.add(legend_label)
            # Insert fields in compare by column into last entry
            final_data["data"][len(final_data["data"]) - 1].update(compare_by_object)
            final_data["numberOfSamples"][len(final_data["numberOfSamples"]) - 1].update(number_of_samples)
    final_data["legendLabels"] = list(legend_label_set)
    final_data['numberOfSamplesXAxis'] = get_number_of_samples_in_year(datamap_reduced, from_year, to_year)

    return JsonResponse(final_data, safe=False)
