import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './base_app/components/App';
import reportWebVitals from './reportWebVitals';
import {createMuiTheme, ThemeProvider} from "@material-ui/core";
import styles from './style-export.module.scss';
import {Provider} from "react-redux";
import {combineReducers, createStore} from "redux";
import {settingsReducer} from "./state/reducers/settingsReducer";
import {filterReducer} from "./state/reducers/filterReducer";
import {dataReducer} from "./state/reducers/dataReducer";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import ESCApp from "./esc_app/ESCApp";

// Create global store for the base app and esc app
export const store = createStore(combineReducers({filter: filterReducer, settings: settingsReducer, data: dataReducer}))
const theme = createMuiTheme({
    palette: {
        primary: {
            // light: styles.colorPrimaryDarker,
            main: styles.colorPrimary,
            // dark: styles.colorPrimaryDarker,
        },
        secondary: {
            // light: styles.colorSecondaryLight,
            main: styles.colorSecondary,
            // dark: styles.colorSecondaryDark,
        },
        error: {
            main: styles.colorError
        },
    },
    typography: {
        fontFamily: ['Neo Sans Pro Light', 'Arial'].join(',')
    }
});

ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <Provider store={store}>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<App/>}/>
                        <Route path="/esc" element={<ESCApp/>}/>
                    </Routes>
                </BrowserRouter>
            </Provider>
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
