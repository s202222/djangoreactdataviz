import {EscFilterState, FilterState} from "./reducers/filterReducer";
import {EscSettingsState, SettingsState} from "./reducers/settingsReducer";
import {DataState, EscDataState} from "./reducers/dataReducer";

// The app has 3 substates, filter, settings and data
// These substates contain the states of both the base app and the esc app
// See more in reducers folder
export type AppState = {
    filter: FilterState & EscFilterState,
    settings: SettingsState & EscSettingsState,
    data: DataState & EscDataState,
}