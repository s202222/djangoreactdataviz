import {AppState} from "../store";

export const selectLineChartData = (state: AppState) => state.data.lineChartData;
export const selectBarChartData = (state: AppState) => state.data.barChartData;
export const selectFilterOptions = (state: AppState) => state.data.filterOptions;
export const selectLegendLabels = (state: AppState) => state.data.legendLabels;
export const selectNumberOfSamples = (state: AppState) => state.data.numberOfSamples;
export const selectNumberOfSamplesXAxis = (state: AppState) => state.data.numberOfSamplesXAxis;


//ESC app
export const selectFilterOptionsEsc = (state: AppState) => state.data.filterOptionsEsc;
export const selectBarChartDataEsc = (state: AppState) => state.data.barChartDataEsc;
export const selectLegendLabelsEsc = (state: AppState) => state.data.legendLabelsEsc;
export const selectNumberOfSamplesEsc = (state: AppState) => state.data.numberOfSamplesEsc;
export const selectNumberOfSamplesXAxisEsc = (state: AppState) => state.data.numberOfSamplesXAxisEsc;