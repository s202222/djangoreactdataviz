import {AppState} from "../store";

export const selectEnableGridX = (state: AppState) => state.settings.enableGridX;
export const selectEnableGridY = (state: AppState) => state.settings.enableGridY;
export const selectMaxAxisValueY = (state: AppState) => state.settings.maxAxisValueY;
export const selectFromYear = (state: AppState) => state.settings.fromYear;
export const selectToYear = (state: AppState) => state.settings.toYear;
export const selectYearRange = (state: AppState) => state.settings.yearRange;
export const selectShowNumberOfSamplesXAxis = (state: AppState) => state.settings.showNumberOfSamplesXAxis;


//ESC app
export const selectEnableGridXEsc = (state: AppState) => state.settings.enableGridXEsc;
export const selectEnableGridYEsc = (state: AppState) => state.settings.enableGridYEsc;
export const selectMaxAxisValueYEsc = (state: AppState) => state.settings.maxAxisValueYEsc;
export const selectFromYearEsc = (state: AppState) => state.settings.fromYearEsc;
export const selectToYearEsc = (state: AppState) => state.settings.toYearEsc;
export const selectYearRangeEsc = (state: AppState) => state.settings.yearRangeEsc;
export const selectShowNumberOfSamplesXAxisEsc = (state: AppState) => state.settings.showNumberOfSamplesXAxisEsc;