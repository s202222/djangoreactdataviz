import _ from "lodash";
import {AppState} from "../store";

export const selectSelectedProps = (state: AppState) => state.filter.selectedProps;
export const selectCompareBy = (state: AppState) => state.filter.compareBy;
export const selectCompareByList = (state: AppState) => _.keys(_.pickBy(state.filter.compareBy));
export const selectChartType = (state: AppState) => state.filter.chartType;

export const selectIsCompareByValid = (state: AppState) => selectCompareByList(state).length > 0;
export const selectIsSelectedPropsValid = (state: AppState) => Object.entries(state.filter.selectedProps).every(([key, value]) => value && value.length > 0 && !value.includes(null) && !value.includes(undefined));

// Filter is valid if at least 1 compareby value is selected and all dropdowns are defined
export const selectIsFilterValid = (state: AppState) => selectIsCompareByValid(state) && selectIsSelectedPropsValid(state)


//ESC app
export const selectSelectedPropsEsc = (state: AppState) => state.filter.selectedPropsEsc;
export const selectIsFilterValidEsc = (state: AppState) => state.filter.selectedPropsEsc.every(value => value.category && value.origin)
