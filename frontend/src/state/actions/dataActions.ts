import {ActionTypes} from "./actionTypes";
import {SamplesDataBarChart, SamplesDataLineChart} from "../reducers/dataReducer";
import {Serie} from "@nivo/line";

export const setLineChartData = (lineChartData: Serie[]) => {
    return {
        type: ActionTypes.SET_LINE_CHART_DATA,
        payload: lineChartData
    }
}

export const setBarChartData = (barChartData: object[]) => {
    return {
        type: ActionTypes.SET_BAR_CHART_DATA,
        payload: barChartData
    }
}

export const setFilterOptions = (filterOptions: string[]) => {
    return {
        type: ActionTypes.SET_FILTER_OPTIONS,
        payload: filterOptions
    }
}

export const setLegendLabels = (legendLabels: string[]) => {
    return {
        type: ActionTypes.SET_LEGEND_LABELS,
        payload: legendLabels
    }
}

export const setNumberOfSamples = (numberOfSamples: SamplesDataLineChart[] | SamplesDataBarChart[]) => {
    return {
        type: ActionTypes.SET_NUMBER_OF_SAMPLES,
        payload: numberOfSamples
    }
}

export const setNumberOfSamplesXAxis = (numberOfSamplesXAxis: object) => {
    return {
        type: ActionTypes.SET_NUMBER_OF_SAMPLES_X_AXIS,
        payload: numberOfSamplesXAxis
    }
}


//ESC app
export const setFilterOptionsEsc = (filterOptions: string[]) => {
    return {
        type: ActionTypes.SET_FILTER_OPTIONS_ESC,
        payload: filterOptions
    }
}

export const setBarChartDataEsc = (barChartData: object[]) => {
    return {
        type: ActionTypes.SET_BAR_CHART_DATA_ESC,
        payload: barChartData
    }
}

export const setLegendLabelsEsc = (legendLabels: string[]) => {
    return {
        type: ActionTypes.SET_LEGEND_LABELS_ESC,
        payload: legendLabels
    }
}

export const setNumberOfSamplesEsc = (numberOfSamples: SamplesDataLineChart[] | SamplesDataBarChart[]) => {
    return {
        type: ActionTypes.SET_NUMBER_OF_SAMPLES_ESC,
        payload: numberOfSamples
    }
}

export const setNumberOfSamplesXAxisEsc = (numberOfSamplesXAxis: object) => {
    return {
        type: ActionTypes.SET_NUMBER_OF_SAMPLES_X_AXIS_ESC,
        payload: numberOfSamplesXAxis
    }
}