export enum ActionTypes {
    // Settings actions
    SWITCH_GRID_X = 'SWITCH_GRID_X',
    SWITCH_GRID_Y = 'SWITCH_GRID_Y',
    SET_MAX_AXIS_VALUE_Y = 'SET_MAX_AXIS_VALUE_Y',
    SET_FROM_YEAR = 'SET_FROM_YEAR',
    SET_TO_YEAR = 'SET_TO_YEAR',
    RESET_SETTINGS = 'RESET_SETTINGS',
    SET_YEAR_RANGE = 'SET_YEAR_RANGE',
    SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS = 'SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS',

    // Filter actions
    SET_SELECTED_PROPS = 'SET_SELECTED_PROPS',
    SET_COMPARE_BY = 'SET_COMPARE_BY',
    SET_CHART_TYPE = 'SET_CHART_TYPE',

    // Data actions
    SET_LINE_CHART_DATA = 'SET_LINE_CHART_DATA',
    SET_BAR_CHART_DATA = 'SET_BAR_CHART_DATA',
    SET_FILTER_OPTIONS = 'SET_FILTER_OPTIONS',
    SET_LEGEND_LABELS = 'SET_LEGEND_LABELS',
    SET_NUMBER_OF_SAMPLES = 'SET_NUMBER_OF_SAMPLES',
    SET_NUMBER_OF_SAMPLES_X_AXIS = 'SET_NUMBER_OF_SAMPLES_X_AXIS',

    //--------ESC app---------
    // Settings actions
    SWITCH_GRID_X_ESC = 'SWITCH_GRID_X_ESC',
    SWITCH_GRID_Y_ESC = 'SWITCH_GRID_Y_ESC',
    SET_MAX_AXIS_VALUE_Y_ESC = 'SET_MAX_AXIS_VALUE_Y_ESC',
    SET_FROM_YEAR_ESC = 'SET_FROM_YEAR_ESC',
    SET_TO_YEAR_ESC = 'SET_TO_YEAR_ESC',
    RESET_SETTINGS_ESC = 'RESET_SETTINGS_ESC',
    SET_YEAR_RANGE_ESC = 'SET_YEAR_RANGE_ESC',
    SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS_ESC = 'SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS_ESC',

    // Filter actions
    SET_SELECTED_PROPS_ESC = 'SET_SELECTED_PROPS_ESC',

    // Data actions
    SET_FILTER_OPTIONS_ESC = 'SET_FILTER_OPTIONS_ESC',
    SET_BAR_CHART_DATA_ESC = 'SET_BAR_CHART_DATA_ESC',
    SET_LEGEND_LABELS_ESC = 'SET_LEGEND_LABELS_ESC',
    SET_NUMBER_OF_SAMPLES_ESC = 'SET_NUMBER_OF_SAMPLES_ESC',
    SET_NUMBER_OF_SAMPLES_X_AXIS_ESC = 'SET_NUMBER_OF_SAMPLES_X_AXIS_ESC',
}