import {ActionTypes} from "./actionTypes";

export const switchGridX = (enableGridX: boolean) => {
    return {
        type: ActionTypes.SWITCH_GRID_X,
        payload: enableGridX
    }
}

export const switchGridY = (enableGridY: boolean) => {
    return {
        type: ActionTypes.SWITCH_GRID_Y,
        payload: enableGridY
    }
}

export const setMaxAxisValueY = (maxValue: number) => {
    return {
        type: ActionTypes.SET_MAX_AXIS_VALUE_Y,
        payload: maxValue
    }
}

export const setFromYear = (year: number) => {
    return {
        type: ActionTypes.SET_FROM_YEAR,
        payload: year
    }
}

export const setToYear = (year: number) => {
    return {
        type: ActionTypes.SET_TO_YEAR,
        payload: year
    }
}

export const setShowNumberOfSamplesXAxis = (showNumberOfSamplesXAxis: boolean) => {
    return {
        type: ActionTypes.SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS,
        payload: showNumberOfSamplesXAxis
    }
}

export const resetSettings = () => {
    return {
        type: ActionTypes.RESET_SETTINGS
    }
}

export const setYearRange = (yearRange: object) => {
    return {
        type: ActionTypes.SET_YEAR_RANGE,
        payload: yearRange
    }
}

export const switchGridXEsc = (enableGridX: boolean) => {
    return {
        type: ActionTypes.SWITCH_GRID_X_ESC,
        payload: enableGridX
    }
}

export const switchGridYEsc = (enableGridY: boolean) => {
    return {
        type: ActionTypes.SWITCH_GRID_Y_ESC,
        payload: enableGridY
    }
}

export const setMaxAxisValueYEsc = (maxValue: number) => {
    return {
        type: ActionTypes.SET_MAX_AXIS_VALUE_Y_ESC,
        payload: maxValue
    }
}

export const setFromYearEsc = (year: number) => {
    return {
        type: ActionTypes.SET_FROM_YEAR_ESC,
        payload: year
    }
}

export const setToYearEsc = (year: number) => {
    return {
        type: ActionTypes.SET_TO_YEAR_ESC,
        payload: year
    }
}

export const setShowNumberOfSamplesXAxisEsc = (showNumberOfSamplesXAxis: boolean) => {
    return {
        type: ActionTypes.SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS_ESC,
        payload: showNumberOfSamplesXAxis
    }
}

export const resetSettingsEsc = () => {
    return {
        type: ActionTypes.RESET_SETTINGS_ESC
    }
}

export const setYearRangeEsc = (yearRange: object) => {
    return {
        type: ActionTypes.SET_YEAR_RANGE_ESC,
        payload: yearRange
    }
}