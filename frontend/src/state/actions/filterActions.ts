import {ActionTypes} from "./actionTypes";

export const setSelectedProps = (selectedPropsChange: Object) => {
    return {
        type: ActionTypes.SET_SELECTED_PROPS,
        payload: selectedPropsChange
    }
}

export const setCompareBy = (compareByChange: Object) => {
    return {
        type: ActionTypes.SET_COMPARE_BY,
        payload: compareByChange
    }
}

export const setChartType = (chartType: string) => {
    return {
        type: ActionTypes.SET_CHART_TYPE,
        payload: chartType
    }
}

//ESC app
export const setSelectedPropsEsc = (selectedPropsChange: Object) => {
    return {
        type: ActionTypes.SET_SELECTED_PROPS_ESC,
        payload: selectedPropsChange
    }
}