import {AnyAction} from "redux";
import {ActionTypes} from "../actions/actionTypes";
import {v4 as uuidv4} from "uuid";

export interface DataProps {
    finding: (string | null)[] | undefined,
    animal: (string | null)[] | undefined,
    origin: (string | null)[] | undefined,
    antibiotic: (string | null)[] | undefined,
}

export interface CompareBy {
    finding: boolean,
    animal: boolean,
    origin: boolean,
    antibiotic: boolean,
}

// Filter state of the base app
export interface FilterState {
    selectedProps: DataProps,
    compareBy: CompareBy,
    chartType: string,
}

const initialFilterState: FilterState & EscFilterState = {
    selectedProps: {
        finding: ["Campylobacter jejuni"],
        animal: ["Cattle", "Human", "Broilers"],
        origin: ["Danish", "Domestic cases", "Travel cases"],
        antibiotic: ["Tetracycline"],
    },
    compareBy: {
        finding: false,
        animal: true,
        origin: true,
        antibiotic: false,
    },
    chartType: 'line',
    selectedPropsEsc: [{
        uuid: uuidv4(),
        category: "Broilers",
        origin: "Danish"
    }],
}

//ESC app
export interface EscSubChartFilterProps {
    uuid: string,
    category: string | null | undefined,
    origin: string | null | undefined,
}

// Filter state of the esc app
export interface EscFilterState {
    selectedPropsEsc: EscSubChartFilterProps[],
}

export function filterReducer(state: (FilterState & EscFilterState) = initialFilterState, action: AnyAction) {
    switch (action.type) {
        case ActionTypes.SET_SELECTED_PROPS:
            return {
                ...state,
                selectedProps: {
                    ...state.selectedProps,
                    ...action.payload
                }
            }
        case ActionTypes.SET_COMPARE_BY:
            return {
                ...state,
                compareBy: {
                    ...state.compareBy,
                    ...action.payload
                }
            }
        case ActionTypes.SET_CHART_TYPE:
            return {...state, chartType: action.payload}
        case ActionTypes.SET_SELECTED_PROPS_ESC:
            return {
                ...state,
                selectedPropsEsc: action.payload
            }
        default:
            return state;
    }
}
