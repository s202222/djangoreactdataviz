import { Serie } from "@nivo/line";
import {AnyAction} from "redux";
import {ActionTypes} from "../actions/actionTypes";

export interface SamplesDataLineChart {
    id: string,
    numberOfSamples: Array<{
        year: string | number,
        samples: string | number
    }>
}

export interface SamplesDataBarChart {
    AAR: string,
    [key: string]: any
}

// Data state of the base app
export interface DataState {
    lineChartData: Serie[],
    barChartData: object[],
    filterOptions: {
        finding: string[],
        animal: string[],
        origin: string[],
        antibiotic: string[],
    },
    legendLabels: string[],
    numberOfSamples: SamplesDataLineChart[] | SamplesDataBarChart[],
    numberOfSamplesXAxis: object
}

const initialDataState: DataState & EscDataState = {
    lineChartData: [],
    barChartData: [],
    filterOptions: {
        finding: [],
        animal: [],
        origin: [],
        antibiotic: []
    },
    legendLabels: [],
    numberOfSamples: [],
    numberOfSamplesXAxis: {},
    barChartDataEsc: [],
    filterOptionsEsc: [],
    legendLabelsEsc: [],
    numberOfSamplesEsc: [],
    numberOfSamplesXAxisEsc: {},
}

//ESC app
// Data state of the esc app
export interface EscDataState {
    barChartDataEsc: Serie[],
    filterOptionsEsc: {
        category: string[],
        origin: string[]
    }[],
    legendLabelsEsc: string[],
    numberOfSamplesEsc: SamplesDataBarChart[],
    numberOfSamplesXAxisEsc: object,
}

export function dataReducer(state: (DataState & EscDataState) = initialDataState, action: AnyAction) {
    switch (action.type) {
        case ActionTypes.SET_LINE_CHART_DATA:
            return {...state, lineChartData: action.payload}
        case ActionTypes.SET_BAR_CHART_DATA:
            return {...state, barChartData: action.payload}
        case ActionTypes.SET_FILTER_OPTIONS:
            return {...state, filterOptions: action.payload}
        case ActionTypes.SET_LEGEND_LABELS:
            return {...state, legendLabels: action.payload}
        case ActionTypes.SET_NUMBER_OF_SAMPLES:
            return {...state, numberOfSamples: action.payload}
        case ActionTypes.SET_NUMBER_OF_SAMPLES_X_AXIS:
            return {...state, numberOfSamplesXAxis: action.payload}
        case ActionTypes.SET_FILTER_OPTIONS_ESC:
            return {...state, filterOptionsEsc: action.payload}
        case ActionTypes.SET_BAR_CHART_DATA_ESC:
            return {...state, barChartDataEsc: action.payload}
        case ActionTypes.SET_LEGEND_LABELS_ESC:
            return {...state, legendLabelsEsc: action.payload}
        case ActionTypes.SET_NUMBER_OF_SAMPLES_ESC:
            return {...state, numberOfSamplesEsc: action.payload}
        case ActionTypes.SET_NUMBER_OF_SAMPLES_X_AXIS_ESC:
            return {...state, numberOfSamplesXAxisEsc: action.payload}
        default:
            return state;
    }
}
