import {AnyAction} from "redux";
import {ActionTypes} from "../actions/actionTypes";

// Settings state of the base app
export interface SettingsState {
    enableGridX: boolean,
    enableGridY: boolean,
    maxAxisValueY: number,
    fromYear: number,
    toYear: number,
    yearRange: {
        minYear: number,
        maxYear: number
    }
    showNumberOfSamplesXAxis: boolean,
}

// Settings state of the esc app
export interface EscSettingsState {
    enableGridXEsc: boolean,
    enableGridYEsc: boolean,
    maxAxisValueYEsc: number,
    fromYearEsc: number,
    toYearEsc: number,
    yearRangeEsc: {
        minYear: number,
        maxYear: number
    }
    showNumberOfSamplesXAxisEsc: boolean,
}

const initialState: SettingsState = {
    enableGridX: false,
    enableGridY: true,
    maxAxisValueY: 100,
    fromYear: 0,
    toYear: 0,
    yearRange: {
        minYear: 0,
        maxYear: 0
    },
    showNumberOfSamplesXAxis: false,
}

const initialStateEsc: EscSettingsState = {
    enableGridXEsc: false,
    enableGridYEsc: true,
    maxAxisValueYEsc: 100,
    fromYearEsc: 0,
    toYearEsc: 0,
    yearRangeEsc: {
        minYear: 0,
        maxYear: 0
    },
    showNumberOfSamplesXAxisEsc: true,
}

export function settingsReducer(state: SettingsState & EscSettingsState = {...initialState, ...initialStateEsc}, action: AnyAction) {
    switch (action.type) {
        case ActionTypes.SWITCH_GRID_X:
            return {...state, enableGridX: action.payload}
        case ActionTypes.SWITCH_GRID_Y:
            return {...state, enableGridY: action.payload}
        case ActionTypes.SET_MAX_AXIS_VALUE_Y:
            return {...state, maxAxisValueY: action.payload}
        case ActionTypes.SET_FROM_YEAR:
            return {...state, fromYear: action.payload}
        case ActionTypes.SET_TO_YEAR:
            return {...state, toYear: action.payload}
        case ActionTypes.SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS:
            return {...state, showNumberOfSamplesXAxis: action.payload}
        case ActionTypes.RESET_SETTINGS:
            return {
                ...state, ...initialState,
                fromYear: state.yearRange.minYear,
                toYear: state.yearRange.maxYear,
                yearRange: state.yearRange
            }
        case ActionTypes.SET_YEAR_RANGE:
            return {...state, yearRange: action.payload}
        case ActionTypes.SWITCH_GRID_X_ESC:
            return {...state, enableGridXEsc: action.payload}
        case ActionTypes.SWITCH_GRID_Y_ESC:
            return {...state, enableGridYEsc: action.payload}
        case ActionTypes.SET_MAX_AXIS_VALUE_Y_ESC:
            return {...state, maxAxisValueYEsc: action.payload}
        case ActionTypes.SET_FROM_YEAR_ESC:
            return {...state, fromYearEsc: action.payload}
        case ActionTypes.SET_TO_YEAR_ESC:
            return {...state, toYearEsc: action.payload}
        case ActionTypes.SET_SHOW_NUMBER_OF_SAMPLES_X_AXIS_ESC:
            return {...state, showNumberOfSamplesXAxisEsc: action.payload}
        case ActionTypes.RESET_SETTINGS_ESC:
            return {
                ...state, ...initialStateEsc,
                fromYearEsc: state.yearRangeEsc.minYear,
                toYearEsc: state.yearRangeEsc.maxYear,
                yearRangeEsc: state.yearRangeEsc
            }
        case ActionTypes.SET_YEAR_RANGE_ESC:
            return {...state, yearRangeEsc: action.payload}
        default:
            return state;
    }
}
