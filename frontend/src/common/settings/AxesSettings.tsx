import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {
    setFromYear,
    setFromYearEsc,
    setMaxAxisValueY,
    setMaxAxisValueYEsc,
    setShowNumberOfSamplesXAxis,
    setShowNumberOfSamplesXAxisEsc,
    setToYear,
    setToYearEsc
} from "../../state/actions/settingsActions";
import {
    List,
    ListItem,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText,
    ListSubheader,
    TextField
} from "@material-ui/core";
import HeightRoundedIcon from '@material-ui/icons/HeightRounded';
import './Settings.scss';
import {
    selectFromYear,
    selectFromYearEsc,
    selectMaxAxisValueY,
    selectMaxAxisValueYEsc,
    selectShowNumberOfSamplesXAxis,
    selectShowNumberOfSamplesXAxisEsc,
    selectToYear,
    selectToYearEsc,
    selectYearRange,
    selectYearRangeEsc
} from "../../state/selectors/settingsSelectors";
import Switch from "@material-ui/core/Switch";
import Filter1Icon from '@material-ui/icons/Filter1';

export const AxesSettings = ({esc}: { esc: boolean }) => {
    const maxAxisValueY = useSelector(esc ? selectMaxAxisValueYEsc : selectMaxAxisValueY)
    const fromYear = useSelector(esc ? selectFromYearEsc : selectFromYear)
    const toYear = useSelector(esc ? selectToYearEsc : selectToYear)
    const yearRange = useSelector(esc ? selectYearRangeEsc : selectYearRange)
    const showNumberOfSamples = useSelector(esc ? selectShowNumberOfSamplesXAxisEsc : selectShowNumberOfSamplesXAxis)
    const dispatch = useDispatch()

    const handleChangeAxisY = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(esc ? setMaxAxisValueYEsc(+event.target.value) : setMaxAxisValueY(+event.target.value))
    };
    const handleChangeFromYear = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(esc ? setFromYearEsc(+event.target.value) : setFromYear(+event.target.value))
        // Set toYear to the new, biggest value of fromYear
        if (+event.target.value > toYear) {
            dispatch(esc ? setToYearEsc(+event.target.value) : setToYear(+event.target.value))
        }
    };
    const handleChangeToYear = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(esc ? setToYearEsc(+event.target.value) : setToYear(+event.target.value))
        // Set fromYear to the new, smaller value of toYear
        if (+event.target.value < fromYear) {
            dispatch(esc ? setFromYearEsc(+event.target.value) : setFromYear(+event.target.value))
        }
    };
    const handleToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(esc ? setShowNumberOfSamplesXAxisEsc(event.target.checked) : setShowNumberOfSamplesXAxis(event.target.checked))
    };

    return (
        <List subheader={<ListSubheader>Axes</ListSubheader>} className="listRoot">
            <ListItem>
                <ListItemIcon>
                    <Filter1Icon/>
                </ListItemIcon>
                <ListItemText id="show-nr-samples-label" primary="Show number of samples"/>
                <ListItemSecondaryAction>
                    <Switch
                        name="showNumberOfSamples"
                        edge="end"
                        onChange={handleToggle}
                        checked={showNumberOfSamples}
                        inputProps={{'aria-labelledby': 'show-nr-samples-label'}}
                    />
                </ListItemSecondaryAction>
            </ListItem>
            <ListItem>
                <ListItemIcon>
                    <HeightRoundedIcon/>
                </ListItemIcon>
                <ListItemText id="y-axis" primary="Max percentage on y axis"/>
                <ListItemSecondaryAction>
                    <TextField
                        id="y-axis-max-input"
                        type="number"
                        className="pctInput"
                        variant="outlined"
                        size="small"
                        inputProps={{
                            step: "5",
                            min: 0,
                            max: 100
                        }}
                        value={maxAxisValueY}
                        onChange={handleChangeAxisY}
                    />
                </ListItemSecondaryAction>
            </ListItem>
            <ListItem className="yearRangeElement">
                <ListItemIcon>
                    <HeightRoundedIcon style={{transform: 'rotate(90deg)'}}/>
                </ListItemIcon>
                <ListItemText id="x-axis" primary="Year range"/>
                <ListItemSecondaryAction>
                    <TextField
                        id="min-year"
                        type="number"
                        className="fromYearInput"
                        variant="outlined"
                        size="small"
                        label='From'
                        inputProps={{
                            min: yearRange.minYear,
                            max: yearRange.maxYear,
                            onKeyDown: (event) => {
                                event.preventDefault();
                            },
                        }}
                        value={fromYear}
                        onChange={handleChangeFromYear}
                    />
                    <TextField
                        id="max-year"
                        type="number"
                        className="toYearInput"
                        variant="outlined"
                        size="small"
                        label='To'
                        inputProps={{
                            min: yearRange.minYear,
                            max: yearRange.maxYear,
                            onKeyDown: (event) => {
                                event.preventDefault();
                            },
                        }}
                        value={toYear}
                        onChange={handleChangeToYear}
                    />
                </ListItemSecondaryAction>
            </ListItem>
        </List>
    );
}
