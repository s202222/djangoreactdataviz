import React from 'react';
import Popover from '@material-ui/core/Popover';
import SettingsRoundedIcon from '@material-ui/icons/SettingsRounded';
import {IconButton, Tooltip, Typography} from "@material-ui/core";
import {GridSettings} from "./GridSettings";
import "./Settings.scss"
import {AxesSettings} from "./AxesSettings";
import RefreshIcon from '@material-ui/icons/Refresh';
import {useDispatch} from "react-redux";
import {resetSettings, resetSettingsEsc} from "../../state/actions/settingsActions";

export const SettingsPopover = ({esc}: {esc: boolean}) => {
    const dispatch = useDispatch()
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    function handleClickReset() {
        dispatch(esc ? resetSettingsEsc() : resetSettings())
    }

    return (
        <div>
            <Tooltip title="Visualization settings" placement="bottom">
                <IconButton aria-describedby={id} color="secondary" onClick={handleClick}>
                    <SettingsRoundedIcon className="settingsIcon"/>
                </IconButton>
            </Tooltip>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                className="settingsPopover"
            >
                <span className="headerContainer">
                    <Typography variant="overline" className="popoverTitle">Visualization settings</Typography>
                    <Tooltip title="Reset settings to default" placement="bottom-end">
                        <IconButton color="secondary" aria-label="reset settings to default" size="small" onClick={handleClickReset}>
                          <RefreshIcon style={{transform: 'scaleX(-1)'}}/>
                        </IconButton>
                    </Tooltip>
                </span>
                <GridSettings esc={esc}/>
                <AxesSettings esc={esc}/>
            </Popover>
        </div>
    );
}
