import React from 'react';
import Switch from '@material-ui/core/Switch';
import {useDispatch, useSelector} from "react-redux";
import {switchGridX, switchGridY, switchGridXEsc, switchGridYEsc} from "../../state/actions/settingsActions";
import {List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, ListSubheader} from "@material-ui/core";
import ViewHeadlineRoundedIcon from '@material-ui/icons/ViewHeadlineRounded';
import './Settings.scss';
import {selectEnableGridX, selectEnableGridY, selectEnableGridXEsc, selectEnableGridYEsc} from "../../state/selectors/settingsSelectors";

export const GridSettings = ({esc}: {esc: boolean}) => {
    const enableGridX = useSelector(esc ? selectEnableGridXEsc : selectEnableGridX)
    const enableGridY = useSelector(esc ? selectEnableGridYEsc: selectEnableGridY)
    const dispatch = useDispatch()

    const handleToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.target.name === 'enableGridX' ?
            dispatch(esc ? switchGridXEsc(event.target.checked) : switchGridX(event.target.checked)) :
            dispatch(esc ? switchGridYEsc(event.target.checked) : switchGridY(event.target.checked))
    };
    return (
        <List subheader={<ListSubheader>Grid</ListSubheader>} className="listRoot">
            <ListItem>
                <ListItemIcon>
                    <ViewHeadlineRoundedIcon style={{transform: 'rotate(90deg)'}}/>
                </ListItemIcon>
                <ListItemText id="switch-list-label-gridX" primary="Vertical grid"/>
                <ListItemSecondaryAction>
                    <Switch
                        name="enableGridX"
                        edge="end"
                        onChange={handleToggle}
                        checked={enableGridX}
                        inputProps={{'aria-labelledby': 'switch-list-label-gridX'}}
                    />
                </ListItemSecondaryAction>
            </ListItem>
            <ListItem>
                <ListItemIcon>
                    <ViewHeadlineRoundedIcon/>
                </ListItemIcon>
                <ListItemText id="switch-list-label-gridY" primary="Horizontal grid"/>
                <ListItemSecondaryAction>
                    <Switch
                        name="enableGridY"
                        edge="end"
                        onChange={handleToggle}
                        checked={enableGridY}
                        inputProps={{'aria-labelledby': 'switch-list-label-gridY'}}
                    />
                </ListItemSecondaryAction>
            </ListItem>
        </List>
    );
}
