import _ from "lodash";
import React from "react";
import {useSelector} from "react-redux";
import {selectCompareBy, selectCompareByList, selectSelectedProps} from "../../../state/selectors/filterSelectors";

export const compareMax = 2;
export const maxSelectedText = (<><br/><i>Max number of options ({compareMax}) is selected</i></>);
export const compareByTooltip = `Select base of comparison; max ${compareMax} values can be selected at a time.`;
export const selectCompareByText = (<><i>Please select a value to compare by.</i></>);
export const selectAllPropsText = (<><i>Please select at least 1 value for each property in the dropdowns.</i></>);

const compareByTooltipTexts = {
    finding: 'Compare bacteria found in samples',
    animal: 'Compare animal/human samples',
    origin: 'Compare origins of samples',
    antibiotic: 'Compare antibiotics tested against a bacterium'
}

function getJoinedString(arrayToJoin?: (string | null)[]) {
    return arrayToJoin?.length! > 1 ?
        arrayToJoin?.slice(0, -1).join(', ').toLowerCase() + ' and ' + arrayToJoin?.slice(-1).join().toLowerCase()
        : arrayToJoin?.join(", ").toLowerCase();
}

const CompareByTooltip = ({field}: { field: string }) => {
    const compareBy = useSelector(selectCompareBy)
    const compareByList = useSelector(selectCompareByList)
    return (
        <>
            {compareByTooltipTexts[field as keyof typeof compareByTooltipTexts]}
            {compareByList.length >= compareMax
            && !compareBy[field as keyof typeof compareBy]
            && maxSelectedText}
        </>
    )
}

export const DropdownTooltips = () => {
    const selectedProps = useSelector(selectSelectedProps)
    return {
        finding: (
            <>
                {selectedProps.finding?.length! > 1 ? "Bacteria" : "Bacterium"} found in
                {" "}<i>{getJoinedString(selectedProps.origin)}</i>
                {" "}<i>{getJoinedString(selectedProps.animal)}</i>
            </>
        ),
        animal: (
            <>
                <i>{_.upperFirst(getJoinedString(selectedProps.origin))}</i>
                {" "}animal sample{selectedProps.animal?.length! > 1 && "s"} in which
                {" "}<i>{getJoinedString(selectedProps.finding)}</i>
                {" "}{selectedProps.finding?.length! > 1 ? "were" : "was"}{" "}found
            </>
        ),
        origin: (
            <>
                Origin{selectedProps.origin?.length! > 1 && "s"} of
                sample{selectedProps.animal?.length! > 1 && "s"} from
                {" "}<i>{getJoinedString(selectedProps.animal)}</i>
            </>
        ),
        antibiotic: (
            <>
                Antibiotic{selectedProps.antibiotic?.length! > 1 && "s"} tested against
                {" "}<i>{getJoinedString(selectedProps.finding)}</i>{" "}found in
                {" "}<i>{getJoinedString(selectedProps.origin)}</i>
                {" "}<i>{getJoinedString(selectedProps.animal)}</i>
            </>
        )
    }
}

export const checkBoxLabels = {
    finding: 'Finding',
    animal: 'Animal category',
    origin: 'Origin',
    antibiotic: 'Antibiotic'
}

export const compareByToolTipComponents = {
    finding: (<CompareByTooltip field='finding'/>),
    animal: (<CompareByTooltip field='animal'/>),
    origin: (<CompareByTooltip field='origin'/>),
    antibiotic: (<CompareByTooltip field='antibiotic'/>)
}