import AutoCompleteMultiple from "./AutoCompleteMultiple";
import AutoCompleteSingle from "./AutoCompleteSingle";
import React from "react";

interface DynamicSelectProps {
    id: string,
    options: string[],
    label: string,
    disabled: boolean,
}

interface DynamicSelectMultipleProps {
    type: 'multiple',
    valueMultiple: (string | null)[] | undefined,
    setValueMultiple: (newValue: (string | null)[] | undefined) => void,
}

interface DynamicSelectSingleProps {
    type: 'single',
    valueSingle: string | null,
    setValueSingle: (newValue: string | null) => void,
}

const DynamicSelect = (props: DynamicSelectProps & (DynamicSelectSingleProps | DynamicSelectMultipleProps)) => {
    return (
        <>
            {props.type === 'multiple' ? (
                <AutoCompleteMultiple
                    {...props}
                    // @ts-ignore
                    value={props.valueMultiple}
                    setValue={props.setValueMultiple}
                />
            ) : (
                <AutoCompleteSingle
                    {...props}
                    value={props.valueSingle}
                    setValue={props.setValueSingle}
                />
            )}
        </>
    );
}

export default DynamicSelect;