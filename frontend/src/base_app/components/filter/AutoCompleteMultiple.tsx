import {Checkbox, FormControlLabel, TextField, Tooltip} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import React from "react";
import {AutoCompleteMultipleProps} from "./AutoCompleteProps";

const AutoCompleteMultiple = ({id, options, label, disabled, value, setValue}: AutoCompleteMultipleProps) => {
    // The max number of values that can be selected from the dropdown (5 is a good choice for bar chart, but for a line chart it can be more)

    const maxToSelect = 8
    return (
        <Autocomplete
            id={id}
            multiple
            disableCloseOnSelect
            limitTags={1}
            size="small"
            fullWidth
            options={options}
            value={value}
            disabled={disabled}
            onChange={(event, newValue) => {
                setValue(newValue)
            }
            }
            renderOption={(option, {selected}) => {
                const numberOfRemainingOptions = maxToSelect - (value?.length || 0);
                return (
                    <Tooltip title={`${numberOfRemainingOptions < 1 ? "No" : numberOfRemainingOptions} 
                    more option${numberOfRemainingOptions !== 1 ? "s" : ""} can be selected`}>
                        <FormControlLabel
                            disabled={value?.length! >= maxToSelect && !selected}
                            control={<Checkbox checked={selected}/>}
                            label={option}
                        />
                    </Tooltip>
                )
            }}
            renderInput={(params: any) => <TextField
                {...params}
                label={`${label} ${value?.length || 0}/${maxToSelect}`}
                variant="outlined"
            />}
        />
    );
}

export default AutoCompleteMultiple;