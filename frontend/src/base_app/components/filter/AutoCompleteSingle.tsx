import {TextField} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import React from "react";
import {AutoCompleteSingleProps} from "./AutoCompleteProps";

const AutoCompleteSingle = ({id, options, label, disabled, value, setValue}: AutoCompleteSingleProps) => {
    return (
        <Autocomplete
            id={id}
            options={options}
            value={value}
            size="small"
            fullWidth
            disabled={disabled}
            onChange={(event, newValue) =>
                setValue(newValue)
            }
            renderInput={(params: any) => <TextField {...params} label={label} variant="outlined"/>}
        />
    );
}
export default AutoCompleteSingle;