export interface AutoCompleteMultipleProps {
    id: string,
    options: string[],
    label: string,
    disabled: boolean,
    value: string[] | undefined,
    setValue: (newValue: string[] | undefined) => void
}

export interface AutoCompleteSingleProps {
    id: string,
    options: string[],
    label: string,
    disabled: boolean,
    value: string | null,
    setValue: (newValue: string | null) => void
}