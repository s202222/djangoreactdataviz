import * as React from 'react';
import ShowChartRoundedIcon from '@material-ui/icons/ShowChartRounded';
import BarChartRoundedIcon from '@material-ui/icons/BarChartRounded';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import './ChartToggle.scss';
import {Tooltip} from "@material-ui/core";

export const ChartToggle = ({
                                defaultType,
                                switchChartType,
                                className
                            }: { defaultType: string, switchChartType: (newChartType: string) => void, className?: string }) => {
    const [chartType, setChartType] = React.useState<string>(defaultType);

    const handleChartType = (
        event: React.MouseEvent<HTMLElement>,
        newChartType: string,
    ) => {
        if (newChartType !== null) {
            setChartType(newChartType);
            switchChartType(newChartType);
        }
    };

    return (
        <ToggleButtonGroup
            value={chartType}
            exclusive
            onChange={handleChartType}
            aria-label="chart type"
            className={className}
        >
            {/*'Selected' needs to be set manually, because after adding the tooltip, it does not render any of the buttons as selected automatically*/}
            <Tooltip title="Line chart" placement="top">
                <ToggleButton value="line" aria-label="line chart" selected={chartType === 'line'}>
                    <ShowChartRoundedIcon/>
                    {/*{'line'}*/}
                </ToggleButton>
            </Tooltip>
            <Tooltip title="Bar chart" placement="top">
                <ToggleButton value="bar" aria-label="bar chart" selected={chartType === 'bar'}>
                    <BarChartRoundedIcon/>
                    {/*{'bar'}*/}
                </ToggleButton>
            </Tooltip>
        </ToggleButtonGroup>
    );
}
