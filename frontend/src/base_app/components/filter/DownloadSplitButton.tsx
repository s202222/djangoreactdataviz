import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import {Tooltip} from "@material-ui/core";
import {useSelector} from "react-redux";
import {
    selectIsCompareByValid,
    selectIsFilterValid,
    selectIsSelectedPropsValid
} from "../../../state/selectors/filterSelectors";
import {selectAllPropsText, selectCompareByText} from "./FilterConsts";

interface SplitButtonProps {
    options: string[],
    handleOptionClick: (event: React.MouseEvent<HTMLLIElement, MouseEvent>, index: number) => void,
    href: string,
    startIcon: React.ReactNode,
    className?: string | undefined,
    esc: boolean
}

function DownloadSplitButton({
                                 options,
                                 handleOptionClick,
                                 href,
                                 startIcon,
                                 className,
                                 esc
                             }: SplitButtonProps) {

    const isFilterValid = useSelector(selectIsFilterValid)
    const isCompareByValid = useSelector(selectIsCompareByValid)
    const isSelectedPropsValid = useSelector(selectIsSelectedPropsValid)

    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLDivElement>(null);
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleMenuItemClick = (
        event: React.MouseEvent<HTMLLIElement, MouseEvent>,
        index: number,
    ) => {
        handleOptionClick(event, index);
        setSelectedIndex(index);
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event: React.MouseEvent<Document, MouseEvent>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }

        setOpen(false);
    };

    return (
        <>
            <ButtonGroup variant="outlined" disableElevation ref={anchorRef} aria-label="split button"
                         className={className} color="primary">
                <Tooltip title={selectedIndex === 0 ?
                    <>Download the complete {esc ? <i>danmap ESC</i> : <i>danmap</i>} dataset as CSV</> :
                    <>Download the currently represented, filtered version of the {esc ? <i>danmap ESC</i> : <i>danmap</i>} dataset as CSV</>}>
                    <Button
                        href={href}
                        // size="small"
                        target="_blank"
                        download
                        startIcon={startIcon}
                    >
                        {options[selectedIndex]}
                    </Button>
                </Tooltip>
                <Tooltip title="Select dataset type">
                    <Button
                        size="small"
                        aria-controls={open ? 'split-button-menu' : undefined}
                        aria-expanded={open ? 'true' : undefined}
                        aria-label="select dataset to download"
                        aria-haspopup="menu"
                        onClick={handleToggle}
                    >
                        <ArrowDropDownIcon/>
                    </Button>
                </Tooltip>
            </ButtonGroup>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition placement="bottom-end">
                {({TransitionProps, placement}) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin: placement === 'bottom-end' ? 'top right' : 'bottom right',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList id="split-button-menu">
                                    {options.map((option, index) => (
                                        <Tooltip title={index === 1 ? (
                                            <>Download filtered dataset
                                                {!isCompareByValid && <><br/>{selectCompareByText}</>}
                                                {!isSelectedPropsValid && <><br/>{selectAllPropsText}</>}
                                            </>) : (<>Download full dataset</>)} placement="right">
                                            <div>
                                                <MenuItem
                                                    key={option}
                                                    selected={index === selectedIndex}
                                                    disabled={option === options[1] && !isFilterValid}
                                                    onClick={(event) => handleMenuItemClick(event, index)}
                                                >
                                                    {option}
                                                </MenuItem>
                                            </div>
                                        </Tooltip>
                                    ))}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </>
    );
}

export default DownloadSplitButton;
