import React, {useEffect, useState} from 'react';
import './App.scss';
import {Checkbox, FormControlLabel, Tooltip} from "@material-ui/core";

import DynamicSelect from "./filter/DynamicSelect";
import GetAppIcon from '@material-ui/icons/GetApp';
import GetAppOutlinedIcon from '@material-ui/icons/GetAppOutlined';
import clsx from "clsx";
import DownloadSplitButton from "./filter/DownloadSplitButton";
import {ChartToggle} from "./filter/ChartToggle";
import LineChart from "./charts/LineChart";
import BarChart from "./charts/BarChart";
import {useDispatch, useSelector} from "react-redux";
import {SettingsPopover} from "../../common/settings/SettingsPopover";
import ExportDialog from "./export/ExportDialog";
import {setChartType, setCompareBy, setSelectedProps} from "../../state/actions/filterActions";
import {checkBoxLabels, compareByToolTipComponents, compareMax, DropdownTooltips} from "./filter/FilterConsts";
import {
    selectChartType,
    selectCompareBy,
    selectCompareByList,
    selectIsFilterValid,
    selectSelectedProps
} from "../../state/selectors/filterSelectors";
import {selectFromYear, selectToYear} from "../../state/selectors/settingsSelectors";
import {selectFilterOptions} from "../../state/selectors/dataSelectors";
import {
    setBarChartData,
    setFilterOptions,
    setLegendLabels,
    setLineChartData,
    setNumberOfSamples,
    setNumberOfSamplesXAxis
} from "../../state/actions/dataActions";
import Typography from "@material-ui/core/Typography";
import FilterListIcon from '@material-ui/icons/FilterList';
import {setFromYear, setToYear, setYearRange} from "../../state/actions/settingsActions";

function App() {
    const dispatch = useDispatch()
    const fromYear = useSelector(selectFromYear)
    const toYear = useSelector(selectToYear)

    const selectedProps = useSelector(selectSelectedProps)
    const compareBy = useSelector(selectCompareBy)
    const compareByList = useSelector(selectCompareByList)
    const chartType = useSelector(selectChartType)

    const filterOptions = useSelector(selectFilterOptions)
    const isFilterValid = useSelector(selectIsFilterValid)

    const [href, setHref] = useState<string>("/danmap.csv");
    const [startIcon, setStartIcon] = useState<React.ReactNode>(<GetAppIcon/>);
    const [showMore, setShowMore] = useState<boolean>(false);

    const legalText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
        "Fusce iaculis felis non tincidunt viverra. Pellentesque gravida id neque in feugiat. Aliquam\n" +
        "euismod, turpis at gravida sagittis, enim velit imperdiet eros, a ornare odio nibh in velit.\n" +
        "Suspendisse malesuada eget metus suscipit suscipit. Cras non finibus diam. Cras eget tellus aliquam,\n" +
        "blandit lectus eget, sollicitudin quam. Praesent at dolor sapien. Nulla vel libero ut lacus interdum\n" +
        "tempor non a arcu. Sed vehicula, quam vitae aliquam aliquet, ante nisi hendrerit tortor, et\n" +
        "scelerisque justo lorem nec massa. Mauris nunc dolor, mattis non feugiat id, iaculis eget odio. Sed\n" +
        "felis ante, blandit eu vehicula a, tincidunt et nibh. Cras efficitur, metus vel sagittis tincidunt,\n" +
        "justo neque venenatis velit, sed interdum dolor arcu vitae nisl.";

    const dataProps = ['finding', 'animal', 'origin', 'antibiotic']

    useEffect(() => {
        fetchYearRange('/api/get_year_range')
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        const filtersReset = {}
        Object.keys(selectedProps).filter(key => !compareByList.includes(key) && selectedProps[key as keyof typeof selectedProps]?.length !== 1)
            .forEach((k: string) => {
                // @ts-ignore
                filtersReset[k] = [selectedProps[k][0]]
            })
        if (Object.keys(filtersReset).length !== 0) {
            dispatch(setSelectedProps(filtersReset))
        }
        // eslint-disable-next-line
    }, [compareByList])


    const createParams = () => {
        let params: any[] = [];
        compareByList.length > 0 && compareByList.forEach(el => params.push(['compare-by', el]))

        //If the selected prop array contains null (i.e. no options are selected), do not add to the params
        dataProps.forEach(prop => !selectedProps[prop as keyof typeof selectedProps]?.includes(null)
            && selectedProps[prop as keyof typeof selectedProps]?.forEach(selected => {
                params.push([prop, selected])
            }))
        params.push(['fromYear', fromYear])
        params.push(['toYear', toYear])
        return params;
    }

    useEffect(() => {
        // Only retrieve any data if at least 1 compareby value is selected is defined
        if (compareByList.length) {
            const params = createParams();
            fetchDataSafely(createURL(`/api/${chartType === 'line' ? 'line_chart_data' : 'dataset'}`, params))
            fetchFilterOptions(createURL(`/api/filter_options`, params))
        }
        // eslint-disable-next-line
    }, [selectedProps, chartType, compareBy])

    useEffect(() => {
        fetchDataSafely(createURL(`/api/${chartType === 'line' ? 'line_chart_data' : 'dataset'}`, createParams()))
        // eslint-disable-next-line
    }, [fromYear, toYear])

    const fetchDataSafely = (url: string) => {
        // Only fetch data if none of the params is undefined, otherwise fetch both data and filter options
        if (!Object.values(selectedProps).some(x => (x === null || x === undefined || x.length < 1 || x.includes(null)))) {
            fetchData(url)
        }
    }

    const switchChartType = (newChartType: string) => {
        dispatch(setChartType(newChartType))
    };

    function createURL(base: string, params: string[][]): string {
        let url = base;
        const searchParams = new URLSearchParams(params).toString();
        if (params.length > 0) {
            url = url.concat(`?${searchParams.toString()}`)
        }
        return url;
    }

    function fetchData(url: string) {
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    dispatch(setLegendLabels(result.legendLabels))
                    dispatch(setNumberOfSamples(result.numberOfSamples))
                    dispatch(setNumberOfSamplesXAxis(result.numberOfSamplesXAxis))
                    if (result.chartType === 'bar')
                        dispatch(setBarChartData(result.data))
                    else if (result.chartType === 'line')
                        dispatch(setLineChartData(result.data))
                }
            )
    }

    function fetchFilterOptions(url: string) {
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    dispatch(setFilterOptions(result))
                }
            )
    }

    function fetchYearRange(url: string) {
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    dispatch(setYearRange(result))
                    dispatch(setFromYear(result.minYear))
                    dispatch(setToYear(result.maxYear))
                }
            )
    }

    function handleDownloadDataset(event: React.MouseEvent<HTMLLIElement, MouseEvent>, index: number) {
        if (index === 0) {
            setHref("/danmap.csv")
            setStartIcon(<GetAppIcon/>)
        } else if (index === 1) {
            setHref(createURL(`/api/filtered_csv`, createParams()))
            setStartIcon(<GetAppOutlinedIcon/>)
        }
    }

    const handleChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(setCompareBy({[event.target.name]: event.target.checked}))
    };

    return (
        <div className="App">
            <div className="mainContainer">
                <div className="filterArea">
                    <div className="headerTitle">
                        <FilterListIcon/>
                        <Typography align='left' className="headerTitleLabel">Filter</Typography>
                    </div>
                    <span className={clsx("selectorContainer")}>
                {dataProps.map(prop => (
                    <>
                        {/*It's surrounded with div-s because it can hold a ref as opposed to the function component.
                    Source: https://stackoverflow.com/questions/57527896/material-ui-tooltip-doesnt-display-on-custom-component-despite-spreading-props*/}
                        <div
                            className={clsx("dynamicSelect", compareBy[prop as keyof typeof compareBy] && "compareBy")}
                            key={prop + '_filter'}
                        >
                            <Tooltip
                                title={compareByToolTipComponents[prop as keyof typeof compareByToolTipComponents]}
                                interactive>
                                <FormControlLabel value={prop}
                                                  control={<Checkbox
                                                      checked={compareBy[prop as keyof typeof compareBy]}
                                                      onChange={handleChecked}
                                                      name={prop}
                                                      disabled={(compareByList.length >= compareMax &&
                                                          !compareBy[prop as keyof typeof compareBy])}/>}
                                                  label=""
                                />
                            </Tooltip>
                            <Tooltip title={DropdownTooltips()[prop as keyof typeof checkBoxLabels]}
                                     placement="top">
                                <DynamicSelect
                                    id={`${prop}-options`}
                                    options={filterOptions[prop as keyof typeof filterOptions].sort()}
                                    label={checkBoxLabels[prop as keyof typeof checkBoxLabels]}
                                    disabled={!compareByList.length || compareByList.length > compareMax}
                                    {...(compareBy[prop as keyof typeof compareBy] ? {
                                        type: 'multiple',
                                        valueMultiple: selectedProps[prop as keyof typeof selectedProps],
                                        setValueMultiple: newValue => dispatch(setSelectedProps({
                                            [prop]: newValue
                                        }))
                                    } : {
                                        type: 'single',
                                        valueSingle: selectedProps[prop as keyof typeof selectedProps]![0],
                                        setValueSingle: newValue => dispatch(setSelectedProps({
                                            [prop]: [newValue]
                                        }))
                                    })}
                                />
                            </Tooltip>
                        </div>
                    </>
                ))}
            </span>
                </div>
                <div className="chartArea">
                    <div className="buttonContainer">
                        <div className="buttonGrLeft">
                            <ExportDialog esc={false}/>
                            <DownloadSplitButton options={["Full dataset", "Filtered dataset"]}
                                                 handleOptionClick={handleDownloadDataset}
                                                 href={href}
                                                 startIcon={startIcon}
                                                 className="downloadSplitBtn"
                                                 esc={false}
                            />
                        </div>
                        <div className="buttonGrRight">
                            <ChartToggle className="chartToggle" defaultType="line" switchChartType={switchChartType}/>
                            <SettingsPopover esc={false}/>
                        </div>
                    </div>
                    <div key={chartType} className={clsx("chartWrapper", !isFilterValid && "disabled")}>
                        {!isFilterValid && (
                            <span className="disabledOverlayText">Please define all filters</span>)}
                        {chartType === 'bar' && (<BarChart isInteractive={isFilterValid}/>)}
                        {chartType === 'line' && (<LineChart isInteractive={isFilterValid}/>)}
                    </div>
                </div>
            </div>
            <div>
                <Typography variant="caption" className="legalText">
                    {showMore ? legalText : `${legalText.substring(0, 300)}... `}
                    <button className="showMoreBtn"
                            onClick={() => setShowMore(!showMore)}>{showMore ? "Show less" : "Show more"}</button>
                    <br/>
                    © 2022 Technical University of Denmark
                </Typography>
            </div>
        </div>
    );
}

export default App;
