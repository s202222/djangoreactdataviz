import React from 'react';
import {createStyles, Theme, withStyles, WithStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import EqualizerIcon from "@material-ui/icons/Equalizer";
import {
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    InputAdornment,
    InputLabel,
    MenuItem,
    Paper,
    PaperProps,
    Select,
    TextField,
    Tooltip
} from "@material-ui/core";
import Draggable from 'react-draggable';
import $ from "jquery";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import _ from "lodash";
import moment from "moment/moment";
import {useSelector} from "react-redux";
import {
    selectChartType,
    selectCompareByList,
    selectIsCompareByValid,
    selectIsFilterValid,
    selectIsSelectedPropsValid,
    selectSelectedProps,
    selectSelectedPropsEsc
} from "../../../state/selectors/filterSelectors";
import "./ExportDialog.scss"
import clsx from "clsx";
import BarChart from "../charts/BarChart";
import LineChart from "../charts/LineChart";
import {selectAllPropsText, selectCompareByText} from "../filter/FilterConsts";
import RefreshIcon from "@material-ui/icons/Refresh";
import StackedBarChart from "../../../esc_app/components/charts/StackedBarChart";

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            padding: theme.spacing(2),
            cursor: 'move'
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const {children, classes, onClose, ...other} = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon/>
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

function PaperComponent(props: PaperProps) {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

export default function ExportDialog({esc}: { esc: boolean }) {

    // For base app
    const selectedProps = useSelector(selectSelectedProps)
    const compareByList = useSelector(selectCompareByList)
    const chartType = useSelector(selectChartType)
    const isFilterValid = useSelector(selectIsFilterValid)
    const isCompareByValid = useSelector(selectIsCompareByValid)
    const isSelectedPropsValid = useSelector(selectIsSelectedPropsValid)
    const propertyLabels = ["Finding(s)", "Animal(s)", "Origin(s)", "Antibiotic(s)"]

    // For ESC app
    const selectedPropsEsc = useSelector(selectSelectedPropsEsc)

    const sourceLabelFixed = "DANMAP"
    const initialState = {
        open: false,
        chartTitle: 'Title',
        chartSubtitle: 'Subtitle',
        sourceLabel: moment().format('YYYY MMM'),
        skipEverySecondXTick: false,
        tickRotation: 0,
        width: 'fullWidth',
        format: 'jpg',
    }
    const [open, setOpen] = React.useState(initialState.open);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
        resetState()
    };

    const [chartTitle, setChartTitle] = React.useState(initialState.chartTitle);
    const handleChangeChartTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChartTitle(event.target.value);
    };

    const [chartSubtitle, setChartSubtitle] = React.useState(initialState.chartSubtitle);
    const handleChangeChartSubtitle = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChartSubtitle(event.target.value);
    };

    const [sourceLabel, setSourceLabel] = React.useState(initialState.sourceLabel);
    const handleChangeSourceLabel = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSourceLabel(event.target.value);
    };

    const [skipEverySecondXTick, setShowEverySecondXTick] = React.useState(initialState.skipEverySecondXTick);
    const handleChangeShowEverySecondXTick = (event: React.ChangeEvent<HTMLInputElement>) => {
        setShowEverySecondXTick(event.target.checked);
    };

    const [tickRotation, setTickRotation] = React.useState(initialState.tickRotation);
    const handleChangeTickRotation = (event: React.ChangeEvent<{ value: unknown }>) => {
        setTickRotation(event.target.value as number);
    };

    const [width, setWidth] = React.useState(initialState.width);
    const handleChangeWidth = (event: React.ChangeEvent<{ value: unknown }>) => {
        setWidth(event.target.value as string);
    };

    const [format, setFormat] = React.useState(initialState.format);
    const handleChangeFormat = (event: React.ChangeEvent<{ value: unknown }>) => {
        setFormat(event.target.value as string);
    };

    const resetState = () => {
        setChartTitle(initialState.chartTitle);
        setChartSubtitle(initialState.chartSubtitle);
        setSourceLabel(initialState.sourceLabel);
        setShowEverySecondXTick(initialState.skipEverySecondXTick);
        setTickRotation(initialState.tickRotation);
        setWidth(initialState.width);
        setFormat(initialState.format);
    }

    const saveAs = (blob: string, fileName: string) => {
        const link = document.createElement("a");
        link.download = fileName;
        link.href = blob;
        link.click();
    }

    const exportImage = () => {
        html2canvas(document.getElementById("componentToPrint")!, {
            scale: 10,
            logging: false
        }).then((canvas) => {
            // document.body.appendChild(canvas);  // if you want see your screenshot in body.
            if (format === 'jpg') {
                const jpeg = canvas.toDataURL("image/jpeg");
                saveAs(jpeg, 'export.jpg')
            }

            if (format === 'png') {
                const png = canvas.toDataURL("image/png");
                saveAs(png, 'export.png')
            }
        })
    }

    function exportPDF() {
        const componentToPrint = $('#componentToPrint');
        const divHeight = componentToPrint.height();
        const divWidth = componentToPrint.width();
        const ratio = divHeight! / divWidth!;
        html2canvas(document.getElementById("componentToPrint")!, {
            scale: 10,
            logging: false
        }).then((canvas) => {
            // document.body.appendChild(canvas);  // if you want see your screenshot in body.
            const jpeg = canvas.toDataURL("image/jpeg");
            const doc = new jsPDF("l", 'px');
            const docWidth = doc.internal.pageSize.getWidth();
            const docHeight = doc.internal.pageSize.getHeight();
            const height = 0.6 * docHeight;
            const width = height / ratio;
            const fontName = "helvetica";
            const fontStyle = "";
            const leftMargin = 25;
            const upperMargin = 30;
            const valueTextsStartX = 75;
            const compareByStartY = 60;
            const compareByBottomMargin = 25;
            const valueTextSpaceY = 15;
            const titleFontSize = 16;
            const textFontSize = 10;
            const legalTextFontSize = 9;
            const numOfValueTexts = Object.keys(selectedProps).length;
            doc.setFont(fontName, fontStyle, "normal")
                .setFontSize(titleFontSize)
                .text("[Title of document]", docWidth / 2 - 50, upperMargin)
            if (!esc) {
                doc.setFontSize(textFontSize).setFont(fontName, fontStyle, "bold")
                    .text(`Compare by: `, leftMargin, compareByStartY)
                    .setFont(fontName, fontStyle, "normal")
                    .text(compareByList.map(el => _.upperFirst(el)).toString(), valueTextsStartX, compareByStartY)
                for (let i = 0; i < numOfValueTexts; i++) {
                    const y = compareByStartY + compareByBottomMargin + i * valueTextSpaceY;
                    doc.setFont(fontName, fontStyle, "bold")
                        .text(`${propertyLabels[i]}: `, leftMargin, y)
                        .setFont(fontName, fontStyle, "normal")
                        // @ts-ignore
                        .text(`${selectedProps[Object.keys(selectedProps)[i]]!.join(", ")}`, valueTextsStartX, y)
                }
                doc.addImage(jpeg, 'JPEG', docWidth/2 - width/2, docHeight/2 - height/2 + numOfValueTexts * valueTextSpaceY,
                width, height);
            }
            else {
                doc.addImage(jpeg, 'JPEG', docWidth/2 - width/2, docHeight/2 - height/2,
                width, height);
            }

            doc.setFont(fontName, fontStyle, "italic")
                .setFontSize(legalTextFontSize)
                .text("[Placeholder for legal text, copyright, source etc.]", leftMargin, docHeight - upperMargin / 2)
                .text(`${moment()}`, docWidth - 140, docHeight - upperMargin / 2)
            // Download the rendered PDF.
            doc.save(esc ? 'danmap_esc_chart.pdf' : 'danmap_chart.pdf');
        })
    }

    function handleClickReset() {
        resetState()
    }

    return (
        <div>
            <Tooltip title={
                <>
                    Export the generated chart with the selected values to PDF
                    {!isCompareByValid && <><br/>{selectCompareByText}</>}
                    {!isSelectedPropsValid && <><br/>{selectAllPropsText}</>}
                </>
            } interactive>
                <span>
                    <Button className="btnExportPDF" variant="outlined" disableElevation onClick={handleClickOpen}
                            color="primary"
                            startIcon={<EqualizerIcon/>}
                            disabled={!isFilterValid}
                    >
                        Export chart
                    </Button>
                </span>
            </Tooltip>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}
                    PaperComponent={PaperComponent} maxWidth="lg">
                <DialogTitle id="draggable-dialog-title" onClose={handleClose}>
                    Export configuration
                </DialogTitle>
                <DialogContent dividers className="dialogContentContainer">
                    <div className={clsx("settingsElement", "previewContainer")}>
                        <span className="previewTitleContainer">
                            <Divider flexItem/>
                            <Typography className="previewTitle">Preview</Typography>
                            <Divider flexItem/>
                        </span>
                        <div id="componentToPrint"
                             className={clsx("contentToPrint", width === 'halfWidth' && 'halfPage')}>
                            <Typography className="sourceLabel">{sourceLabelFixed} {sourceLabel}</Typography>
                            {!esc &&
                            <>
                                <Typography className="chartTitle">{chartTitle}</Typography>
                                <Typography className="chartSubTitle">{chartSubtitle}</Typography>
                            </>
                            }
                            <div key={`${chartType}-preview`}
                                 className='chartArea'>
                                {chartType === 'bar' && (
                                    <BarChart isInteractive={false} tickRotation={tickRotation}
                                              skipEverySecondXTick={skipEverySecondXTick}/>)}
                                {chartType === 'line' && (
                                    <LineChart isInteractive={false} tickRotation={tickRotation}
                                               skipEverySecondXTick={skipEverySecondXTick}/>)}
                                {esc && (
                                    <>
                                        <div className="chartHeader">
                                            {selectedPropsEsc.map(({uuid, category, origin}, i) =>
                                                <div key={uuid + "_subchart_title"}>
                                                    <Typography className="mainTitle">{category}</Typography>
                                                    <Typography className="subTitle">{origin}</Typography>
                                                </div>
                                            )}
                                        </div>
                                        <StackedBarChart isInteractive={false} tickRotation={tickRotation}
                                                         skipEverySecondXTick={skipEverySecondXTick}/>
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                    <Divider orientation="vertical" flexItem/>
                    <div className="settingsContainer">
                        <span className={clsx("settingsTitleContainer", "settingsElement")}>
                            <Typography className="settingsTitle">Settings</Typography>
                            <Tooltip title="Reset settings to default" placement="bottom-end">
                                <IconButton color="secondary" aria-label="reset settings to default" size="small"
                                            onClick={handleClickReset}>
                                  <RefreshIcon style={{transform: 'scaleX(-1)'}}/>
                                </IconButton>
                            </Tooltip>
                        </span>
                        {!esc &&
                        <>
                            <TextField
                                id="chart-title-input"
                                label="Chart title"
                                className="settingsElement"
                                size="small"
                                value={chartTitle}
                                onChange={handleChangeChartTitle}
                            />
                            <TextField
                                id="chart-subtitle-input"
                                label="Chart subtitle"
                                className="settingsElement"
                                size="small"
                                value={chartSubtitle}
                                onChange={handleChangeChartSubtitle}
                            />
                        </>
                        }
                        <TextField
                            id="source-label-input"
                            label="Source label"
                            className="settingsElement"
                            size="small"
                            value={sourceLabel}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">{sourceLabelFixed}</InputAdornment>,
                            }}
                            onChange={handleChangeSourceLabel}
                        />
                        <TextField
                            id="tick-rotation-input"
                            label="Tick rotation"
                            className="settingsElement"
                            size="small"
                            value={tickRotation}
                            type="number"
                            onChange={handleChangeTickRotation}
                        />
                        <FormControlLabel
                            label="Skip every second year"
                            className="settingsElement"
                            control={
                                <Checkbox
                                    checked={skipEverySecondXTick}
                                    onChange={handleChangeShowEverySecondXTick}
                                    name="showEverySecondXTick"
                                    color="primary"
                                    size="small"
                                />
                            }
                        />
                        <FormControl variant="outlined" className="settingsElement" size="small">
                            <InputLabel id="width-selector-label">Width</InputLabel>
                            <Select
                                labelId="width-selector-label"
                                id="width-selector"
                                value={width}
                                onChange={handleChangeWidth}
                                label="Width"
                            >
                                <MenuItem value={'halfWidth'}>1/2 page (8.75cm)</MenuItem>
                                <MenuItem value={'fullWidth'}>Full page (18cm)</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl variant="outlined" className="settingsElement" size="small">
                            <InputLabel id="format-selector-label">Format</InputLabel>
                            <Select
                                labelId="format-selector-label"
                                id="format-selector"
                                value={format}
                                onChange={handleChangeFormat}
                                label="Format"
                            >
                                <MenuItem value={'jpg'}>JPG</MenuItem>
                                <MenuItem value={'png'}>PNG</MenuItem>
                                <MenuItem value={'pdf'}>PDF</MenuItem>
                            </Select>
                        </FormControl>
                        {format === 'pdf' && !esc && (
                            <FormHelperText className="pdfHelperText">The PDF file includes the selected filter values
                                as well.</FormHelperText>)}
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={format === 'pdf' ? exportPDF : exportImage} color="primary" disableElevation
                            variant="contained">
                        Export as {format}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
