// install (please make sure versions match peerDependencies)
// yarn add @nivo/core @nivo/bar
import {ResponsiveBar} from '@nivo/bar'
import {AxisTickProps} from '@nivo/axes'
import CustomLegend from "./CustomLegend";
import "./BarChart.scss";
import {LegendColors, LegendFallbackColors} from "./LegendColors";
import {useSelector} from "react-redux";
import {selectBarChartData, selectLegendLabels, selectNumberOfSamples} from "../../../state/selectors/dataSelectors";
import {
    selectEnableGridX,
    selectEnableGridY,
    selectMaxAxisValueY,
    selectShowNumberOfSamplesXAxis
} from "../../../state/selectors/settingsSelectors";
import {CustomTick} from "./CustomTick";
import {SamplesDataBarChart} from "../../../state/reducers/dataReducer";

// Make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.

const BarChart = ({isInteractive, tickRotation, skipEverySecondXTick}: { isInteractive?: boolean, tickRotation?: number, skipEverySecondXTick?: boolean }) => {

    // Legendlabels is the list of values that are compared (e.g. the 5 antibiotic selected)
    const legendLabels = useSelector(selectLegendLabels)
    const enableGridX = useSelector(selectEnableGridX)
    const enableGridY = useSelector(selectEnableGridY)
    const maxAxisValueY = useSelector(selectMaxAxisValueY)
    const barChartData = useSelector(selectBarChartData)
    const showNumberOfSamplesXAxis = useSelector(selectShowNumberOfSamplesXAxis)
    const numberOfSamples = useSelector(selectNumberOfSamples) as SamplesDataBarChart[]

    const getLegendColor = (label: string, idx: number) => {
        const label_split = label.split(' - ')
        const possibleLegendColors = Object.keys(LegendColors).filter((key) =>
            key.startsWith(label_split[0]) || key.startsWith(label_split[1]));
        return LegendColors[label as keyof typeof LegendColors]
            || LegendColors[possibleLegendColors[0] as keyof typeof LegendColors]
            || LegendFallbackColors[idx]
    }

    const getNrOfSamplesForBar = (year: string | number, id: string | number) => {
        return numberOfSamples.filter((value) => value.AAR === year)[0][id]
    }

    return (
        barChartData.length > 0 ? (
            <div className="barChart" key="bar-chart">
                <ResponsiveBar
                    theme={{
                        fontFamily: 'Arial',
                        tooltip: {
                            container: {
                                background: "rgba(255,255,255,0.85)",
                            },
                        },
                    }}
                    key='responsiveBar'
                    data={barChartData}
                    keys={legendLabels}
                    indexBy="AAR"
                    margin={{top: 24, right: 16, bottom: 80, left: 60}}
                    padding={0.3}
                    // this fixed the bug where I changed the antibiotic from 1 to another and it only showed 2 bars
                    groupMode="grouped"
                    // place the labels on top of the bars
                    labelFormat={labelValue => (
                        (<tspan className="label" y={-8}>{labelValue}%</tspan>) as unknown
                    ) as string}
                    labelSkipWidth={36}
                    valueScale={{type: 'linear'}}
                    indexScale={{type: 'band', round: true}}
                    colors={legendLabels.map(getLegendColor)}
                    minValue={0}
                    maxValue={maxAxisValueY}
                    enableGridX={enableGridX}
                    enableGridY={enableGridY}
                    borderColor={{from: 'color', modifiers: [['darker', 1.6]]}}
                    axisTop={null}
                    axisRight={null}
                    axisBottom={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: tickRotation || 0,
                        legend: 'Year',
                        legendOffset: showNumberOfSamplesXAxis ? 56 : 44,
                        legendPosition: 'middle',
                        renderTick: (tick: AxisTickProps<string>) => CustomTick(tick, skipEverySecondXTick),
                    }}
                    axisLeft={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: '% resistant isolates',
                        legendPosition: 'middle',
                        legendOffset: -40
                    }}
                    layers={["grid", "axes", "bars", "markers"]}
                    isInteractive={isInteractive}
                    // id: type of antibiotics
                    // value: the percentage
                    // index: 0, 1, 2...
                    // indexValue: 2011, 2012...
                    // color: hex code
                    tooltip={({id, value, index, indexValue, color, data}) => {
                        return (
                            <div style={{color}}>
                                <strong>{id}</strong> - {indexValue} - {value}% - <i>({getNrOfSamplesForBar(indexValue, id)})</i>
                            </div>
                        )
                    }}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                />
                <CustomLegend key="bar-legend"
                              legendData={legendLabels.map((el: string, idx: number) => ({
                                  label: el,
                                  color: getLegendColor(el, idx)
                              }))}/>
            </div>
        ) : (<div/>)
    )
};

export default BarChart;