import "./CustomLegend.scss";

export interface LegendElement {
    label: string,
    color: string
}

const CustomLegend = ({legendData}: { legendData: LegendElement[] }) => {

    const legendIcon = (color: string, scale = 1) => (
        <svg width={40 * scale} height={30 * scale} xmlns="http://www.w3.org/2000/svg">
            <g>
                <g id="svg_3">
                    <ellipse ry={4.7 * scale} rx={4.7 * scale} id="svg_1" cy={15 * scale} cx={20 * scale} fill={color}/>
                    <rect stroke="null" rx={2 * scale} id="svg_2" height={2.2 * scale} width={34.25 * scale}
                          y={14 * scale} x={3 * scale} fill={color}/>
                </g>
            </g>
        </svg>
    );

    const legendText = (text: string) => (
        <span
            style={{
                fontSize: '8.5pt',
                fontFamily: 'Arial'
            }}
        >
          {text}
        </span>
    );

    return (
        <div className="legendContainer">
            {legendData.map((element) => (
                <div
                    className="legendElement"
                    key={element.label}
                >
                    {legendIcon(element.color, 1)}
                    {legendText(element.label)}
                </div>
            ))}
        </div>
    );
};

export default CustomLegend;
