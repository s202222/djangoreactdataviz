import {AxisTickProps} from '@nivo/axes'
import {useSelector} from "react-redux";
import {selectNumberOfSamplesXAxis} from "../../../state/selectors/dataSelectors";
import {selectShowNumberOfSamplesXAxis} from "../../../state/selectors/settingsSelectors";

export const CustomTick = (tick: AxisTickProps<string>, skipEverySecond?: boolean) => {
    const numberOfSamplesXAxis = useSelector(selectNumberOfSamplesXAxis)
    const showNumberOfSamplesXAxis = useSelector(selectShowNumberOfSamplesXAxis)

    const ticksToShow = skipEverySecond ? Object.keys(numberOfSamplesXAxis).map((value: any, index) => index % 2 === 0 ? value : '')
        : Object.keys(numberOfSamplesXAxis)

    return (
        <g transform={`translate(${tick.x},${tick.y + 22})`}>
            <line stroke="rgb(160, 160, 160)" strokeWidth={1} y1={-22} y2={-16}/>
            <text
                transform={`rotate(${tick.rotate})`}
                textAnchor="middle"
                dominantBaseline="middle"
                style={{
                    fill: '#333',
                    fontSize: 11,
                    fontFamily: 'Arial',
                }}
            >
                {ticksToShow.find(vts => vts === tick.value.toString()) ?
                    <>
                        <tspan x="0">{tick.value}</tspan>
                        {showNumberOfSamplesXAxis && (
                            <tspan x="0" dy="1.2em">
                                ({numberOfSamplesXAxis[tick.value as keyof typeof numberOfSamplesXAxis]})
                            </tspan>
                        )}
                    </> : <tspan x="0">{""}</tspan>}
            </text>
        </g>
    )
}