import {ResponsiveLine} from '@nivo/line'
import {AxisTickProps} from '@nivo/axes'
import {LegendColors, LegendFallbackColors} from "./LegendColors";
import CustomLegend from "./CustomLegend";
import "./BarChart.scss";
import styles from "../../../style-export.module.scss";
import {useSelector} from "react-redux";
import {selectLegendLabels, selectLineChartData, selectNumberOfSamples,} from "../../../state/selectors/dataSelectors";
import {
    selectEnableGridX,
    selectEnableGridY,
    selectMaxAxisValueY,
    selectShowNumberOfSamplesXAxis
} from "../../../state/selectors/settingsSelectors";
import {CustomTick} from "./CustomTick";
import {SamplesDataLineChart} from "../../../state/reducers/dataReducer";
// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.

const LineChart = ({isInteractive, tickRotation, skipEverySecondXTick}:
                       { isInteractive?: boolean, tickRotation?: number, skipEverySecondXTick?: boolean }) => {
    const legendLabels = useSelector(selectLegendLabels)
    const enableGridX = useSelector(selectEnableGridX)
    const enableGridY = useSelector(selectEnableGridY)
    const maxAxisValueY = useSelector(selectMaxAxisValueY)
    const lineChartData = useSelector(selectLineChartData)
    const showNumberOfSamplesXAxis = useSelector(selectShowNumberOfSamplesXAxis)
    const numberOfSamples = useSelector(selectNumberOfSamples) as SamplesDataLineChart[]

    const getLegendColor = (label: string, idx: number) => {
        const label_split = label.split(' - ')
        const possibleLegendColors = Object.keys(LegendColors).filter((key) =>
            key.startsWith(label_split[0]) || key.startsWith(label_split[1]));
        return LegendColors[label as keyof typeof LegendColors]
            || LegendColors[possibleLegendColors[0] as keyof typeof LegendColors]
            || LegendFallbackColors[idx]
    }

    const getNrOfSamplesForPoint = (year: string | number, id: string | number) => {
        return numberOfSamples.filter((value) => value.id === id)[0].numberOfSamples.filter(value => value.year === +year)[0].samples
    }

    return (
        lineChartData.length > 0 ? (
            <div className="barChart" key="line-chart">
                <ResponsiveLine
                    theme={{
                        fontFamily: 'Arial'
                    }}
                    key='responsiveLine'
                    data={lineChartData}
                    margin={{top: 24, right: 16, bottom: 60, left: 60}}
                    xScale={{type: 'point'}}
                    yScale={{type: 'linear', min: 0, max: maxAxisValueY, stacked: false, reverse: false}}
                    yFormat=" >-.1f"
                    colors={legendLabels.map(getLegendColor)}
                    enableGridX={enableGridX}
                    enableGridY={enableGridY}
                    axisTop={null}
                    axisRight={null}
                    axisBottom={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: tickRotation || 0,
                        legend: 'Year',
                        legendOffset: showNumberOfSamplesXAxis ? 56 : 44,
                        legendPosition: 'middle',
                        renderTick: (tick: AxisTickProps<string>) => CustomTick(tick, skipEverySecondXTick),
                    }}
                    axisLeft={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: '% resistant isolates',
                        legendOffset: -40,
                        legendPosition: 'middle'
                    }}
                    pointSize={8}
                    pointColor={{from: 'color'}}
                    pointBorderWidth={2}
                    pointBorderColor={{from: 'serieColor'}}
                    pointLabelYOffset={-12}
                    useMesh={true}
                    isInteractive={isInteractive}
                    sliceTooltip={({slice}) => {
                        return (
                            <div
                                style={{
                                    background: "rgba(255,255,255,0.9)",
                                    padding: '9px 12px',
                                    border: '1px solid #ddd',
                                    textAlign: 'left',
                                }}
                            >
                                {/*Display year on top*/}
                                <div style={{
                                    color: styles.colorPrimaryDarker,
                                    padding: '4px 0',
                                    textAlign: 'center'
                                }}
                                >
                                    <strong>{slice.points[0]?.data.x}</strong>
                                </div>
                                {/*Map on the reverse copy of the array*/}
                                {slice.points.slice(0).reverse().map(point => (
                                    <div
                                        key={point.id}
                                        style={{
                                            color: point.serieColor,
                                            padding: '3px 0',
                                        }}
                                    >
                                        <strong>{point.serieId}</strong> {point.data.yFormatted}% <i>({getNrOfSamplesForPoint(slice.points[0]?.data.x.toString(), point.serieId)})</i>
                                    </div>
                                ))}
                            </div>
                        )
                    }}
                    enableSlices="x"
                />
                <CustomLegend key="line-legend" legendData={legendLabels.map((el: string, idx: number) => ({
                    label: el,
                    color: getLegendColor(el, idx)
                }))}/>
            </div>
        ) : (<div/>)
    )
}
export default LineChart;