import Typography from "@material-ui/core/Typography";
import clsx from "clsx";
import ExportDialog from "../base_app/components/export/ExportDialog";
import DownloadSplitButton from "../base_app/components/filter/DownloadSplitButton";
import {SettingsPopover} from "../common/settings/SettingsPopover";
import React, {useEffect, useState} from "react";
import GetAppIcon from "@material-ui/icons/GetApp";
import BarChartIcon from '@material-ui/icons/BarChart';
import "../base_app/components/App.scss";
import "./ESCApp.scss";
import SubChartFilter from "./components/subchart_filter/SubChartFilter";
import Button from "@material-ui/core/Button";
import {v4 as uuidv4} from "uuid";
import {useDispatch, useSelector} from "react-redux";
import {selectFilterOptionsEsc} from "../state/selectors/dataSelectors";
import {selectIsFilterValidEsc, selectSelectedPropsEsc} from "../state/selectors/filterSelectors";
import {
    setBarChartDataEsc,
    setFilterOptionsEsc,
    setLegendLabelsEsc,
    setNumberOfSamplesEsc,
    setNumberOfSamplesXAxisEsc
} from "../state/actions/dataActions";
import {selectFromYearEsc, selectToYearEsc} from "../state/selectors/settingsSelectors";
import {setSelectedPropsEsc} from "../state/actions/filterActions";
import StackedBarChart from "./components/charts/StackedBarChart";
import {cloneDeep} from 'lodash';
import {Tooltip} from "@material-ui/core";
import {setFromYearEsc, setToYearEsc, setYearRangeEsc} from "../state/actions/settingsActions";
import GetAppOutlinedIcon from "@material-ui/icons/GetAppOutlined";

function ESCApp() {
    const dispatch = useDispatch()
    const fromYear = useSelector(selectFromYearEsc)
    const toYear = useSelector(selectToYearEsc)

    const filterOptions = useSelector(selectFilterOptionsEsc)
    const selectedProps = useSelector(selectSelectedPropsEsc)

    const queryProps = ['category', 'origin']

    const isSubChartSelectionValid = useSelector(selectIsFilterValidEsc)

    const [href, setHref] = useState<string>("/danmap_ESC_select.csv");
    const [startIcon, setStartIcon] = useState<React.ReactNode>(<GetAppIcon/>);
    const [showMore, setShowMore] = useState<boolean>(false);

    const legalText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
        "Fusce iaculis felis non tincidunt viverra. Pellentesque gravida id neque in feugiat. Aliquam\n" +
        "euismod, turpis at gravida sagittis, enim velit imperdiet eros, a ornare odio nibh in velit.\n" +
        "Suspendisse malesuada eget metus suscipit suscipit. Cras non finibus diam. Cras eget tellus aliquam,\n" +
        "blandit lectus eget, sollicitudin quam. Praesent at dolor sapien. Nulla vel libero ut lacus interdum\n" +
        "tempor non a arcu. Sed vehicula, quam vitae aliquam aliquet, ante nisi hendrerit tortor, et\n" +
        "scelerisque justo lorem nec massa. Mauris nunc dolor, mattis non feugiat id, iaculis eget odio. Sed\n" +
        "felis ante, blandit eu vehicula a, tincidunt et nibh. Cras efficitur, metus vel sagittis tincidunt,\n" +
        "justo neque venenatis velit, sed interdum dolor arcu vitae nisl.";


    useEffect(() => {
        fetchYearRange('/api/esc/get_year_range')
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        const params = createParams();
        fetchDataSafely(createURL('/api/esc/chart_data', params))
        fetchFilterOptions(createURL(`api/esc/filter_options`, params))
        // eslint-disable-next-line
    }, [selectedProps])

    useEffect(() => {
        fetchDataSafely(createURL('/api/esc/chart_data', createParams()))
        // eslint-disable-next-line
    }, [fromYear, toYear])

    const createParams = () => {
        let params: any[] = [];
        //If the selected prop array contains null (i.e. no options are selected), do not add to the params
        selectedProps.forEach(selProp => {
            queryProps.forEach(qProp => {
                if (selProp[qProp as keyof typeof selectedProps[0]] != null) {
                    params.push([qProp, selProp[qProp as keyof typeof selectedProps[0]]])
                }
            })
        })

        params.push(['fromYear', fromYear])
        params.push(['toYear', toYear])
        return params;
    }

    function createURL(base: string, params: string[][]): string {
        let url = base;
        const searchParams = new URLSearchParams(params).toString();
        if (params.length > 0) {
            url = url.concat(`?${searchParams.toString()}`)
        }
        return url;
    }

    function handleDownloadDataset(event: React.MouseEvent<HTMLLIElement, MouseEvent>, index: number) {
        if (index === 0) {
            setHref("/danmap_ESC_select.csv")
            setStartIcon(<GetAppIcon/>)
        } else if (index === 1) {
            setHref(createURL(`/api/esc/filtered_csv`, createParams()))
            setStartIcon(<GetAppOutlinedIcon/>)
        }
    }

    function handleAddSubChartClick() {
        dispatch(setSelectedPropsEsc(
            [
                ...selectedProps,
                {
                    uuid: uuidv4(),
                    category: filterOptions[0].category[0],
                    origin: filterOptions[0].origin[0]
                }
            ]
        ))
    }

    function handleRemoveSubChart(id: string) {
        const selectedPropsReduced = selectedProps.filter(prop => prop.uuid !== id)
        dispatch(setSelectedPropsEsc(selectedPropsReduced))
    }

    function fetchFilterOptions(url: string) {
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    dispatch(setFilterOptionsEsc(result))
                }
            )
    }

    const fetchDataSafely = (url: string) => {
        // Only fetch data if none of the params is undefined, otherwise fetch both data and filter options
        if (!selectedProps.some(x => (x === null || x === undefined))) {
            fetchData(url)
        }
    }

    function fetchData(url: string) {
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    dispatch(setLegendLabelsEsc(result.legendLabels))
                    dispatch(setNumberOfSamplesEsc(result.numberOfSamples))
                    dispatch(setNumberOfSamplesXAxisEsc(result.numberOfSamplesXAxis))
                    dispatch(setBarChartDataEsc(result.data))
                }
            )
    }

    function fetchYearRange(url: string) {
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    dispatch(setYearRangeEsc(result))
                    dispatch(setFromYearEsc(result.minYear))
                    dispatch(setToYearEsc(result.maxYear))
                }
            )
    }

    return (
        <div className="App">
            <div className="mainContainer">
                <div className="filterArea">
                    <div className="headerTitle">
                        <BarChartIcon/>
                        <Typography align='left' className="headerTitleLabel">Subcharts</Typography>
                    </div>
                    <span className={clsx("selectorContainer")}>
                    {selectedProps.map(({uuid, category, origin}, i) =>
                        <SubChartFilter key={uuid}
                                        id={uuid}
                                        nr={i + 1}
                                        categoryOptions={filterOptions[i]?.category || []}
                                        originOptions={filterOptions[i]?.origin || []}
                                        categoryValue={category!}
                                        setCategoryValue={(newValue) => {
                                            const selectedPropsClone = cloneDeep(selectedProps);
                                            selectedPropsClone[i] = {
                                                ...selectedPropsClone[i],
                                                'category': newValue
                                            }
                                            dispatch(setSelectedPropsEsc(selectedPropsClone))
                                        }}
                                        originValue={origin!}
                                        setOriginValue={(newValue) => {
                                            const selectedPropsClone = cloneDeep(selectedProps);
                                            selectedPropsClone[i] = {
                                                ...selectedPropsClone[i],
                                                'origin': newValue
                                            }
                                            dispatch(setSelectedPropsEsc(selectedPropsClone))
                                        }}
                                        onRemove={handleRemoveSubChart}/>
                    )}
                        <Tooltip
                            title={selectedProps.length >= 5 ? "Max number of subcharts (5) are defined" : "Add new subcharts"}>
                        <span>
                            <Button variant="outlined" disableElevation onClick={handleAddSubChartClick}
                                    size="small"
                                    color="primary"
                                    disabled={selectedProps.length >= 5}
                            >
                                Add sub chart
                            </Button>
                        </span>
                    </Tooltip>
            </span>
                </div>
                <div className="chartArea">
                    <div className="buttonContainer">
                        <div className="buttonGrLeft">
                            <ExportDialog esc={true}/>
                            <DownloadSplitButton options={["Full dataset", "Filtered dataset"]}
                                                 handleOptionClick={handleDownloadDataset}
                                                 href={href}
                                                 startIcon={startIcon}
                                                 className="downloadSplitBtn"
                                                 esc={true}
                            />
                        </div>
                        <div className="buttonGrRight">
                            <SettingsPopover esc={true}/>
                        </div>
                    </div>
                    <div key="stackedBar" className={clsx("chartWrapper", !isSubChartSelectionValid && "disabled")}>
                        <div className="chartHeader">
                            {selectedProps.map(({uuid, category, origin}, i) =>
                                <div key={uuid + "_subchart_title"}>
                                    <Typography className="mainTitle">{category}</Typography>
                                    <Typography className="subTitle">{origin}</Typography>
                                </div>
                            )}
                        </div>
                        {!isSubChartSelectionValid && (
                            <span className="disabledOverlayText">Please define valid sub-charts</span>)}
                        <StackedBarChart isInteractive={isSubChartSelectionValid}/>
                    </div>
                </div>
            </div>
            <div>
                <Typography variant="caption" className="legalText">
                    {showMore ? legalText : `${legalText.substring(0, 300)}... `}
                    <button className="showMoreBtn"
                            onClick={() => setShowMore(!showMore)}>{showMore ? "Show less" : "Show more"}</button>
                    <br/>
                    © 2022 Technical University of Denmark
                </Typography>
            </div>
        </div>
    );
}

export default ESCApp;