import "./CustomLegendEsc.scss";

export interface LegendElement {
    label: string,
    color: string
}

const CustomLegendEsc = ({legendData}: { legendData: LegendElement[] }) => {

    const legendIcon = (color: string) => (
        <svg width="10" height="10" xmlns="http://www.w3.org/2000/svg">
            <rect height="10" width="10" fill={color}/>
        </svg>
    );

    const legendText = (text: string) => (
        <span
            style={{
                fontSize: '8.5pt',
                fontFamily: 'Arial'
            }}
        >
          {text}
        </span>
    );

    return (
        <div className="legendContainerEsc">
            {legendData.map((element) => (
                <div
                    className="legendElementEsc"
                    key={element.label}
                >
                    {legendIcon(element.color)}
                    {legendText(element.label)}
                </div>
            ))}
        </div>
    );
};

export default CustomLegendEsc;
