export enum LegendColorsEsc {
    'Other/Not available' = '#97979b',
    'AmpC phenotype' = '#76933c',
    'ESBL phenotype' = '#8db4e2',
    'ESBL-AmpC phenotype' = '#31869b'
}