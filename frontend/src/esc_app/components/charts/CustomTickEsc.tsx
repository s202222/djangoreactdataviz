import {AxisTickProps} from '@nivo/axes'
import {useSelector} from "react-redux";
import {selectNumberOfSamplesEsc, selectNumberOfSamplesXAxisEsc} from "../../../state/selectors/dataSelectors";
import {selectShowNumberOfSamplesXAxisEsc} from "../../../state/selectors/settingsSelectors";

export const CustomTickEsc = (tick: AxisTickProps<string>, skipEverySecond?: boolean) => {
    const numberOfSamplesXAxis = useSelector(selectNumberOfSamplesXAxisEsc)
    const numberOfSamples = useSelector(selectNumberOfSamplesEsc)
    const showNumberOfSamplesXAxis = useSelector(selectShowNumberOfSamplesXAxisEsc)

    function skipYearsInSubCharts() {
        let lastCharacter = '0';
        let groupedYears = numberOfSamples.reduce((previousValue, currentItem) => {
            const year = currentItem.AAR
            if (year.endsWith(lastCharacter)) {
                if (!previousValue[lastCharacter]) {
                    previousValue[lastCharacter] = [];
                }
                previousValue[lastCharacter].push(year);
            } else {
                lastCharacter = year.slice(-1);
                if (!previousValue[lastCharacter]) {
                    previousValue[lastCharacter] = [];
                }
                previousValue[lastCharacter].push(year);
            }
            return previousValue;
        }, {} as Record<string, string[]>)
        Object.keys(groupedYears).forEach(function (key, index) {
            groupedYears[key] = groupedYears[key].map((value: any, index) => index % 2 === 0 ? value : '');
        });
        return Object.values(groupedYears).flat()
    }

    const ticksToShow = skipEverySecond ? skipYearsInSubCharts() : Object.keys(numberOfSamplesXAxis)

    return (
        <g transform={`translate(${tick.x},${tick.y + 22})`}>
            <line stroke="rgb(160, 160, 160)" strokeWidth={1} y1={-22} y2={-16}/>
            <text
                transform={`rotate(${tick.rotate})`}
                textAnchor="middle"
                dominantBaseline="middle"
                style={{
                    fill: '#333',
                    fontSize: 11,
                    fontFamily: 'Arial',
                }}
            >
                {ticksToShow.find(vts => vts === tick.value.toString()) ?
                    <>
                        <tspan x="0">{tick.value.slice(0, 4)}</tspan>
                        {showNumberOfSamplesXAxis && (
                            <tspan x="0" dy="1.2em">
                                ({numberOfSamplesXAxis[tick.value as keyof typeof numberOfSamplesXAxis]})
                            </tspan>
                        )}
                    </> : <tspan x="0">{""}</tspan>}
            </text>
        </g>
    )
}