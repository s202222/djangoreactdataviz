// install (please make sure versions match peerDependencies)
// yarn add @nivo/core @nivo/bar
import {ResponsiveBar} from '@nivo/bar'
import {AxisTickProps} from '@nivo/axes'
import "./../../../base_app/components/charts/BarChart.scss";
import {useSelector} from "react-redux";
import {
    selectBarChartDataEsc,
    selectLegendLabelsEsc,
    selectNumberOfSamplesEsc
} from "../../../state/selectors/dataSelectors";
import {
    selectEnableGridXEsc,
    selectEnableGridYEsc,
    selectMaxAxisValueYEsc,
    selectShowNumberOfSamplesXAxisEsc
} from "../../../state/selectors/settingsSelectors";
import {SamplesDataBarChart} from "../../../state/reducers/dataReducer";
import {LegendFallbackColors} from "../../../base_app/components/charts/LegendColors";
import {CustomTickEsc} from "./CustomTickEsc";
import CustomLegendEsc from "./CustomLegendEsc";
import {LegendColorsEsc} from "./LegendColorsEsc";

// Make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.


// This chart is used in the ESC app
const StackedBarChart = ({
                      isInteractive,
                      tickRotation,
                      skipEverySecondXTick
                  }: { isInteractive?: boolean, tickRotation?: number, skipEverySecondXTick?: boolean }) => {

    // Legendlabels is the list of values that are compared (e.g. the 5 antibiotic selected)
    const legendLabels = useSelector(selectLegendLabelsEsc)
    const enableGridX = useSelector(selectEnableGridXEsc)
    const enableGridY = useSelector(selectEnableGridYEsc)
    const maxAxisValueY = useSelector(selectMaxAxisValueYEsc)
    const stackedBarChartData = useSelector(selectBarChartDataEsc)
    const showNumberOfSamplesXAxis = useSelector(selectShowNumberOfSamplesXAxisEsc)
    const numberOfSamples = useSelector(selectNumberOfSamplesEsc) as SamplesDataBarChart[]

    const getLegendColor = (label: string, idx: number) => {
        const label_split = label.split(' - ')
        const possibleLegendColors = Object.keys(LegendColorsEsc).filter((key) =>
            key.startsWith(label_split[0]) || key.startsWith(label_split[1]));
        return LegendColorsEsc[label as keyof typeof LegendColorsEsc]
            || LegendColorsEsc[possibleLegendColors[0] as keyof typeof LegendColorsEsc]
            || LegendFallbackColors[idx]
    }

    const getNrOfSamplesForBar = (year: string | number, id: string | number) => {
        return numberOfSamples.filter((value) => value.AAR === year)[0][id]
    }

    return (
        stackedBarChartData.length > 0 ? (
            <div className="barChart" key="bar-chart">
                <ResponsiveBar
                    theme={{
                        fontFamily: 'Arial',
                        tooltip: {
                            container: {
                                background: "rgba(255,255,255,0.85)",
                            },
                        },
                    }}
                    key='stackedBar'
                    data={stackedBarChartData}
                    keys={legendLabels}
                    indexBy="AAR"
                    margin={{top: 24, right: 16, bottom: 80, left: 60}}
                    padding={0.3}
                    // place the labels on top of the bars
                    // labelFormat={labelValue => (
                    //     (<tspan className="label" y={-8}>{labelValue}%</tspan>) as unknown
                    // ) as string}
                    // labelSkipWidth={36}
                    enableLabel={false}
                    valueScale={{type: 'linear'}}
                    indexScale={{type: 'band', round: true}}
                    colors={legendLabels.map(getLegendColor)}
                    minValue={0}
                    maxValue={maxAxisValueY}
                    enableGridX={enableGridX}
                    enableGridY={enableGridY}
                    borderColor={{from: 'color', modifiers: [['darker', 1.6]]}}
                    axisTop={null}
                    axisRight={null}
                    axisBottom={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: tickRotation || 0,
                        legend: 'Year',
                        legendOffset: showNumberOfSamplesXAxis ? 56 : 44,
                        legendPosition: 'middle',
                        renderTick: (tick: AxisTickProps<string>) => CustomTickEsc(tick, skipEverySecondXTick),
                    }}
                    axisLeft={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: '% samples with ESBL or AmpC-producing E. coli',
                        legendPosition: 'middle',
                        legendOffset: -40
                    }}
                    layers={["grid", "axes", "bars", "markers"]}
                    isInteractive={isInteractive}
                    tooltip={({id, value, index, indexValue, color, data}) => {
                        return (
                            <div style={{color}}>
                                <strong>{id}</strong> - {indexValue} - {value}%
                                - <i>({getNrOfSamplesForBar(indexValue, id)})</i>
                            </div>
                        )
                    }}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                />
                <CustomLegendEsc key="bar-legend"
                              legendData={legendLabels.map((el: string, idx: number) => ({
                                  label: el,
                                  color: getLegendColor(el, idx)
                              }))}/>
            </div>
        ) : (<div/>)
    )
};

export default StackedBarChart;