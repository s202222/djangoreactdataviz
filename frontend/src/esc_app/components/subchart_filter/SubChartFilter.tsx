import Autocomplete from "@material-ui/lab/Autocomplete";
import {TextField} from "@material-ui/core";
import React from "react";
import "./SubChartFilter.scss";
import IconButton from "@material-ui/core/IconButton";
import {SubChartFilterProps} from "./SubChartFilterProps";
import {MdClose} from "react-icons/all";

export default function SubChartFilter({
                                           id,
                                           nr,
                                           categoryOptions,
                                           originOptions,
                                           categoryValue,
                                           setCategoryValue,
                                           originValue,
                                           setOriginValue,
                                           onRemove
                                       }: SubChartFilterProps) {

    return (
        <div className="subChartFilterContainer">
            <span className="subChartFilterLabelContainer">
                <span className="subChartFilterLabel">Sub Chart - {nr}</span>
                {nr !== 1 && (
                    <IconButton size="small" aria-label="remove" color="secondary" onClick={() => onRemove(id)}>
                        <MdClose/>
                    </IconButton>
                )}
            </span>
            <div className="subChartFilterSelector">
                <Autocomplete
                    id={"category-options"}
                    options={categoryOptions}
                    value={categoryValue}
                    size="small"
                    fullWidth
                    // disabled={disabled}
                    onChange={(event, newValue) =>
                        setCategoryValue(newValue)
                    }
                    renderInput={(params: any) => <TextField {...params} label={"Animal Category"}
                                                             variant="outlined"/>}
                />
                <Autocomplete
                    id={"origin-options"}
                    options={originOptions}
                    value={originValue}
                    size="small"
                    fullWidth
                    // disabled={disabled}
                    onChange={(event, newValue) =>
                        setOriginValue(newValue)
                    }

                    renderInput={(params: any) => <TextField {...params} label={"Origin"} variant="outlined"/>}
                />
            </div>
        </div>
    );
}