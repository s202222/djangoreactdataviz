export interface SubChartFilterProps {
    // Unique id (uuid)
    id: string,
    nr: number,
    categoryOptions: string[],
    originOptions: string[],
    categoryValue: string | null,
    setCategoryValue: (newValue: string | null) => void,
    originValue: string | null,
    setOriginValue: (newValue: string | null) => void,
    onRemove: (id: string) => void,
}