## DANMAP visualisation app

Link to the _base app_: http://food-pwebs01.win.dtu.dk

Link to the _ESC app_: http://food-pwebs01.win.dtu.dk/esc

Note: at the moment it is only accessible through DTU network, i.e. not public to the internet

#### Short description
This app is intented to provide a visualisation platform for the [DANMAP](https://www.danmap.org/) data.

- Play around with the DANMAP data by adjusting the highly customisable filters
- Export your custom charts for reports / presentations (full page/half page in the formats of JPG, PNG or PDF)
- Download the full or filtered data as CSV
- etc.

#### Technical details

##### Tech Stack

- Frontend: [React](https://reactjs.org/), [React MUI](https://mui.com/), [Redux](https://redux.js.org/), [Typescript](https://www.typescriptlang.org/), [Nivo](https://nivo.rocks/) (for data visualisation)

- Web server / Web server gateway interface: [NGiNX](https://www.nginx.com/), [uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/)

- Backend: [Django](https://www.djangoproject.com/), [Django REST Framework](https://www.django-rest-framework.org/), [Python](https://www.python.org/)

Server is setup based on the following tutorial:
https://tonyteaches.tech/django-nginx-uwsgi-tutorial/

Integrate React frontend with Django backend: https://youtu.be/F9o4GSkSo40

##### Run the app
**Backend**

- Open a new terminal window
- Navigate to the root of the project (where the manage.py file is located)
- run `py manage.py runserver`

**Frontend**

- Open another terminal window
- Navigate to the `frontend` folder
- run `yarn start`

By default the app runs on localhost:3000.

##### Deploy new changes to the server

1. Push local changes here
2. Pull changes on "food-pwebs01.win.dtu.dk" server here: '/home/s202222/djangoreactdataviz'
3. Navigate into "frontend" folder
4. `yarn install`, then `yarn build`
5. restart system service: `sudo systemctl restart emperor.uwsgi.service`
6. Done! Now you should see the changes on the live site.